example\_contact package
========================

Subpackages
-----------

.. toctree::

    example_contact.tests

Submodules
----------

example\_contact.base module
----------------------------

.. automodule:: example_contact.base
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.celery module
------------------------------

.. automodule:: example_contact.celery
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.dev\_greg module
---------------------------------

.. automodule:: example_contact.dev_greg
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.dev\_malcolm module
------------------------------------

.. automodule:: example_contact.dev_malcolm
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.dev\_patrick module
------------------------------------

.. automodule:: example_contact.dev_patrick
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.dev\_test module
---------------------------------

.. automodule:: example_contact.dev_test
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.urls module
----------------------------

.. automodule:: example_contact.urls
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.views module
-----------------------------

.. automodule:: example_contact.views
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.wsgi module
----------------------------

.. automodule:: example_contact.wsgi
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: example_contact
    :members:
    :undoc-members:
    :show-inheritance:
