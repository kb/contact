example\_contact.tests package
==============================

Submodules
----------

example\_contact.tests.test\_management\_command module
-------------------------------------------------------

.. automodule:: example_contact.tests.test_management_command
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.tests.test\_view module
----------------------------------------

.. automodule:: example_contact.tests.test_view
    :members:
    :undoc-members:
    :show-inheritance:

example\_contact.tests.test\_view\_perm module
----------------------------------------------

.. automodule:: example_contact.tests.test_view_perm
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: example_contact.tests
    :members:
    :undoc-members:
    :show-inheritance:
