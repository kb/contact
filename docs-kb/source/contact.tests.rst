contact.tests package
=====================

Submodules
----------

contact.tests.factories module
------------------------------

.. automodule:: contact.tests.factories
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_api module
------------------------------

.. automodule:: contact.tests.test_api
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact module
----------------------------------

.. automodule:: contact.tests.test_contact
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact\_address module
-------------------------------------------

.. automodule:: contact.tests.test_contact_address
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact\_connection module
----------------------------------------------

.. automodule:: contact.tests.test_contact_connection
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact\_email module
-----------------------------------------

.. automodule:: contact.tests.test_contact_email
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact\_phone module
-----------------------------------------

.. automodule:: contact.tests.test_contact_phone
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_contact\_user module
----------------------------------------

.. automodule:: contact.tests.test_contact_user
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_gender module
---------------------------------

.. automodule:: contact.tests.test_gender
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_management\_command module
----------------------------------------------

.. automodule:: contact.tests.test_management_command
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_search module
---------------------------------

.. automodule:: contact.tests.test_search
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_tasks module
--------------------------------

.. automodule:: contact.tests.test_tasks
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_view module
-------------------------------

.. automodule:: contact.tests.test_view
    :members:
    :undoc-members:
    :show-inheritance:

contact.tests.test\_view\_perm module
-------------------------------------

.. automodule:: contact.tests.test_view_perm
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: contact.tests
    :members:
    :undoc-members:
    :show-inheritance:
