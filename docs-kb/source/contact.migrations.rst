contact.migrations package
==========================

Submodules
----------

contact.migrations.0001\_initial module
---------------------------------------

.. automodule:: contact.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0002\_default\_gender module
-----------------------------------------------

.. automodule:: contact.migrations.0002_default_gender
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0003\_contact\_subscribed module
---------------------------------------------------

.. automodule:: contact.migrations.0003_contact_subscribed
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0004\_auto\_20180207\_1522 module
----------------------------------------------------

.. automodule:: contact.migrations.0004_auto_20180207_1522
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0005\_auto\_20180417\_0734 module
----------------------------------------------------

.. automodule:: contact.migrations.0005_auto_20180417_0734
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0006\_auto\_20180417\_0736 module
----------------------------------------------------

.. automodule:: contact.migrations.0006_auto_20180417_0736
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0007\_auto\_20180417\_0737 module
----------------------------------------------------

.. automodule:: contact.migrations.0007_auto_20180417_0737
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0008\_auto\_20180723\_1937 module
----------------------------------------------------

.. automodule:: contact.migrations.0008_auto_20180723_1937
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0009\_auto\_20180916\_1617 module
----------------------------------------------------

.. automodule:: contact.migrations.0009_auto_20180916_1617
    :members:
    :undoc-members:
    :show-inheritance:

contact.migrations.0010\_remove\_contact\_subscribed module
-----------------------------------------------------------

.. automodule:: contact.migrations.0010_remove_contact_subscribed
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: contact.migrations
    :members:
    :undoc-members:
    :show-inheritance:
