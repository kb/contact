.. contact documentation master file, created by
   sphinx-quickstart on Mon Oct 29 09:20:25 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

contact
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
