contact
*******

Django contact application

.. tip:: Combine with the
         https://github.com/pkimber/ember-data-error
         project to try and understand Ember!
         Comment out ``authentication_classes`` and ``permission_classes``
         from ``SoftDeleteViewSet`` (``contact/api.py``).

To Do
=====

- Do we want a ``slug`` field in the ``Contact`` model.  I think we probably
  do.  13/12/2016, PJK, I am using the ``User``, ``username`` field.
- Need to think about how to link users to the contact model.  The contact
  *is a* user.  The ``UserContact`` field links one or more users to a contact.
  Need to write some tests for this.  The original system only allowed a user
  to be linked to one contact.  I think this should be changed to allow a user
  to be linked to any number of contacts.
- The migration is currently losing the ``industry`` field.  See
  ``0006_auto_20151220_2207.py`` for the commented out field.
- Remove ``checkout`` methods from ``Contact``.
- What is the ``nationality`` field doing on the contact?
- What is the ``deleted`` field doing on the contact?

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-contact
  # or
  python3 -m venv venv-contact

  source venv-contact/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

If you **do not** have Elasticsearch installed::

  pytest -x -m "not elasticsearch"

.. note:: We should add Elasticsearch to our GitLab testing configuration, so
          we don't have to include / exclude Elasticsearch tests:
          https://www.docker.elastic.co/

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
