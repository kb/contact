# -*- encoding: utf-8 -*-
import logging

from django.db import transaction
from rest_framework.exceptions import APIException, ParseError
from rest_framework_json_api import serializers

from contact.models import (
    Contact,
    ContactAddress,
    ContactConnection,
    ContactEmail,
    ContactError,
    ContactPhone,
)
from contact.tasks import update_contact_index

logger = logging.getLogger(__name__)


class TagListSerializer(serializers.Field):
    def to_internal_value(self, data):
        if type(data) is not list:
            raise ParseError("expected a list of data")

        return data

    def to_representation(self, obj):
        if type(obj) is not list:
            return [tag.name for tag in obj.all()]

        return obj


class ModelTagSerializer(serializers.ModelSerializer):
    """Base class for a model with tags."""

    tags = TagListSerializer(required=False)

    def create(self, validated_data):
        tags = validated_data.pop("tags")
        with transaction.atomic():
            instance = super().create(validated_data)
            for x in tags:
                instance.tags.add(x)
            transaction.on_commit(
                lambda: update_contact_index.send_with_options(
                    args=(instance.pk,)
                )
            )
        return instance

    def update(self, instance, validated_data):
        tags = validated_data.pop("tags", [])
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.tags.clear()
            for x in tags:
                instance.tags.add(x)
            transaction.on_commit(
                lambda: update_contact_index.send_with_options(
                    args=(instance.pk,)
                )
            )
        return instance


class ContactAddressSerializer(ModelTagSerializer):
    class Meta:
        model = ContactAddress
        fields = (
            "id",
            "contact",
            "tags",
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
            "notes",
            "access_code",
            "access",
        )


class ContactConnectionSerializer(ModelTagSerializer):
    class Meta:
        model = ContactConnection
        fields = ("id", "contact", "tags", "connection", "notes")


class ContactEmailSerializer(ModelTagSerializer):
    class Meta:
        model = ContactEmail
        fields = ("id", "contact", "tags", "email", "notes")


class ContactPhoneSerializer(ModelTagSerializer):
    class Meta:
        model = ContactPhone
        fields = ("id", "contact", "tags", "phone", "notes")


class ContactSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(
        source="user.first_name", allow_blank=True, required=False
    )
    last_name = serializers.CharField(
        source="user.last_name", allow_blank=True, required=False
    )
    addresses = serializers.SerializerMethodField()
    connections = serializers.SerializerMethodField()
    emails = serializers.SerializerMethodField()
    phones = serializers.SerializerMethodField()

    class Meta:
        model = Contact
        fields = (
            "id",
            "company_name",
            "title",
            "last_name",
            "first_name",
            "notes",
            "addresses",
            "connections",
            "phones",
            "emails",
        )

    def _create_unique_user(self, first_name, last_name):
        try:
            return Contact.objects.create_unique_user(first_name, last_name)
        except ContactError as e:
            logger.exception(e)
            raise APIException(
                "To create a unique user you must enter a "
                "company name, first name or last name"
            )

    def _user(self, user):
        email = user.get("email", "")
        first_name = user.get("first_name", "")
        last_name = user.get("last_name", "")
        return email, first_name, last_name

    def create(self, validated_data):
        instance = None
        user_data = validated_data.pop("user", {})
        email, first_name, last_name = self._user(user_data)
        company_name = validated_data.get("company_name")
        with transaction.atomic():
            user = self._create_unique_user(
                first_name, last_name or company_name
            )
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            instance = Contact.objects.create_contact(
                user=user, **validated_data
            )
            transaction.on_commit(
                lambda: update_contact_index.send_with_options(
                    args=(instance.pk,)
                )
            )
        return instance

    def get_addresses(self, contact):
        qs = (
            ContactAddress.objects.current()
            .filter(contact=contact)
            .order_by("created")
        )
        serializer = ContactAddressSerializer(instance=qs, many=True)
        return serializer.data

    def get_connections(self, contact):
        qs = (
            ContactConnection.objects.current()
            .filter(contact=contact)
            .order_by("created")
        )
        serializer = ContactConnectionSerializer(instance=qs, many=True)
        return serializer.data

    def get_emails(self, contact):
        qs = (
            ContactEmail.objects.current()
            .filter(contact=contact)
            .order_by("created")
        )
        serializer = ContactEmailSerializer(instance=qs, many=True)
        return serializer.data

    def get_phones(self, contact):
        qs = (
            ContactPhone.objects.current()
            .filter(contact=contact)
            .order_by("created")
        )
        serializer = ContactPhoneSerializer(instance=qs, many=True)
        return serializer.data

    def update(self, instance, validated_data):
        user_data = validated_data.pop("user")
        email, first_name, last_name = self._user(user_data)
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            user = instance.user
            user.email = email or ""
            user.first_name = first_name or ""
            user.last_name = last_name or ""
            user.save()
            transaction.on_commit(
                lambda: update_contact_index.send_with_options(
                    args=(instance.pk,)
                )
            )
        return instance
