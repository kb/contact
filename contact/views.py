# -*- encoding: utf-8 -*-
from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
    UserPassesTestMixin,
)
from django.contrib.auth import get_user_model, REDIRECT_FIELD_NAME
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.text import slugify
from django.views.generic import CreateView, ListView, UpdateView
from taggit.models import Tag

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin, RedirectNextMixin
from gdpr.models import Consent, UserConsent
from .forms import (
    ContactAddressEmptyForm,
    ContactAddressForm,
    ContactEmailEmptyForm,
    ContactEmailForm,
    ContactEmptyForm,
    ContactForm,
    ContactMailingForm,
    ContactPhoneEmptyForm,
    ContactPhoneForm,
    GenderForm,
)
from .models import (
    check_perm_contact,
    Contact,
    ContactAddress,
    ContactEmail,
    ContactError,
    ContactPhone,
    Gender,
)
from .tasks import update_contact_index


class ContactExtraMixin(RedirectNextMixin):
    """Add the contact to the context and redirect to the detail view.

    .. note:: The class is named ``Extra`` because it is designed as a base
              class for editing the *extra* data e.g. phone numbers, emails
              and postal addresses.

    """

    def _show_tags(self):
        return self.kwargs["show_tags"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(contact=self._contact())
        return context

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return url_with_querystring(
                reverse("contact.detail", args=[self.object.contact.pk]),
                next=next_url,
            )
        else:
            return reverse("contact.detail", args=[self.object.contact.pk])


class ContactExtraCreateMixin(ContactExtraMixin):
    def _contact(self):
        pk = self.kwargs.get("pk")
        return Contact.objects.get(pk=pk)

    def _tag_from_url(self):
        result = None
        tag = self.kwargs.get("tag", None)
        if tag:
            try:
                slug = slugify(tag)
                result = [Tag.objects.get(slug=slug)]
            except Tag.DoesNotExist:
                raise ContactError(
                    "Tag '{}' does not exist.  It needs to be created before "
                    "it can be used".format(slug)
                )
        else:
            raise ContactError("Please include a tag in the URL")
        return result

    def _tags(self, form):
        show_tags = self._show_tags()
        if show_tags:
            result = form.cleaned_data.get("tag")
        else:
            result = self._tag_from_url()
        return result

    def form_valid(self, form):
        with transaction.atomic():
            tags = self._tags(form)
            self.object = form.save(commit=False)
            self.object.contact = self._contact()
            result = super().form_valid(form)
            for x in tags:
                self.object.tags.add(x)
        return result

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {"all_tags": self._all_tags(), "show_tags": self._show_tags()}
        )
        return kwargs


class ContactExtraDeleteMixin(ContactExtraMixin):
    def _contact(self):
        return self.object.contact


class ContactExtraUpdateMixin(ContactExtraMixin):
    def _contact(self):
        return self.object.contact

    def _save_tags(self, form):
        """Update tags if they were visible on the form."""
        show_tags = self._show_tags()
        if show_tags:
            tags = form.cleaned_data.get("tag")
            self.object.tags.clear()
            for x in tags:
                self.object.tags.add(x)

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            self._save_tags(form)
            result = super().form_valid(form)
        return result

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {"all_tags": self._all_tags(), "show_tags": self._show_tags()}
        )
        return kwargs

    def get_initial(self):
        result = super().get_initial()
        tag = [x.pk for x in self.object.tags.all()]
        result.update(dict(tag=tag))
        return result


class ContactAddressCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraCreateMixin,
    BaseMixin,
    CreateView,
):
    """Create an address for a contact.

    This view works in two modes:

    1. ``show_tags=True`` where the user will be able to select and deselect
       address tags e.g. ``Invoice``, ``Delivery``.
    2. ``show_tags=False`` where the tag will be from the ``kwargs`` on the URL.

    """

    form_class = ContactAddressForm
    model = ContactAddress

    def _all_tags(self):
        return ContactAddress.tags.all().order_by("name")


class ContactAddressDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraDeleteMixin,
    BaseMixin,
    UpdateView,
):
    model = ContactAddress
    form_class = ContactAddressEmptyForm
    template_name = "contact/contact_address_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())


class ContactAddressUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraUpdateMixin,
    BaseMixin,
    UpdateView,
):
    """Update a contact address.

    This view works in two modes:

    1. ``show_tags=True`` where the user will be able to select and deselect
       address tags e.g. ``Invoice``, ``Delivery``.
    2. ``show_tags=False`` where the tags will not be visible and will not be
       changed by the view.

    """

    model = ContactAddress
    form_class = ContactAddressForm

    def _all_tags(self):
        return ContactAddress.tags.all().order_by("name")


class ContactPermMixin(UserPassesTestMixin):
    """Check the contact for the ticket matches the user."""

    def test_contact(self):
        raise NotImplementedError(
            "{0} is missing implementation of the "
            "test_contact method. You should write "
            "one.".format(self.__class__.__name__)
        )

    def test_func(self, user):
        """A member of staff or the contact for the ticket has access."""
        return check_perm_contact(user, self.test_contact())


class ContactCreateUpdateMixin:

    def _is_auto_generate_user(self):
        return hasattr(self, "auto_generate_user") and callable(
            self.auto_generate_user
        )

    def _is_create_user(self):
        """Are we creating or updating the user model?"""
        result = False
        if not hasattr(self.object, "user") or not self.object.user:
            result = True
        return result

    def _is_save_extra(self):
        return hasattr(self, "save_extra") and callable(self.save_extra)

    def _save_address(self, form, tag):
        address_one = form.cleaned_data.get("address_one")
        address_two = form.cleaned_data.get("address_two")
        locality = form.cleaned_data.get("locality")
        town = form.cleaned_data.get("town")
        admin_area = form.cleaned_data.get("admin_area")
        postal_code = form.cleaned_data.get("postal_code")
        country = form.cleaned_data.get("country")
        if (
            address_one
            or address_two
            or locality
            or town
            or admin_area
            or postal_code
            or country
        ):
            address = self.object.primary_address
            if not address:
                address = ContactAddress(contact=self.object)

            address.address_one = address_one
            address.address_two = address_two
            address.locality = locality
            address.town = town
            address.admin_area = admin_area
            address.postal_code = postal_code
            address.country = country
            address.save()
            address.tags.add(tag)

    def _save_phone(self, tag, phone):
        contact_phone = self.object.current_phone(tag=tag)
        if not contact_phone:
            contact_phone = ContactPhone(contact=self.object, phone=phone)
        contact_phone.phone = phone
        contact_phone.save()
        contact_phone.tags.add(tag)

    def _save_landline(self, form):
        phone = form.cleaned_data.get("phone")
        if phone:
            self._save_phone(tag=ContactPhone.LANDLINE, phone=phone)

    def _save_mobile(self, form):
        mobile = form.cleaned_data.get("mobile")
        if mobile:
            self._save_phone(tag=ContactPhone.MOBILE, phone=mobile)

    def _save_email(self, form):
        email = form.cleaned_data.get("email")
        if email:
            contact_email = self.object.current_email()
            if not contact_email:
                contact_email = ContactEmail(contact=self.object, email=email)
            else:
                contact_email.email = email
            contact_email.save()

    def _save_user(self, form):
        if self._is_create_user():
            self.object.user = get_user_model().objects.create_user(
                form.cleaned_data["username"]
            )
            self.object.user.is_active = False
            self.object.user.set_unusable_password()
        else:
            user_name = form.cleaned_data.get("username")
            if user_name:
                self.object.user.username = user_name
        self.object.user.first_name = form.cleaned_data["first_name"]
        self.object.user.last_name = form.cleaned_data["last_name"]
        self.object.user.save()

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            if self._is_auto_generate_user():
                if self._is_create_user():
                    self.auto_generate_user(form)
            else:
                self._save_user(form)
            result = super().form_valid(form)
            self._save_email(form)
            self._save_landline(form)
            self._save_mobile(form)
            self._save_address(form, ContactAddress.PRIMARY)
            if self._is_save_extra():
                self.save_extra(form)
            transaction.on_commit(
                lambda: update_contact_index.send_with_options(
                    args=(self.object.pk,)
                )
            )
        return result


class ContactCreateMailingView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactCreateUpdateMixin,
    BaseMixin,
    CreateView,
):
    form_class = ContactMailingForm
    model = Contact

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            result = super().form_valid(form)
            consent = Consent.objects.get(
                slug=Contact.GDPR_CONSENT_CONTACT_MAILING
            )
            UserConsent.objects.set_consent(
                consent, True, self.object.user, user_updated=self.request.user
            )
        return result

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": False})
        return kwargs

    def get_success_url(self):
        return reverse("contact.mailing.list")


class ContactCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactCreateUpdateMixin,
    BaseMixin,
    CreateView,
):
    form_class = ContactForm
    model = Contact

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": False})
        return kwargs


class ContactDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
):
    form_class = ContactEmptyForm
    model = Contact
    template_name = "contact/contact_delete_form.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("contact.list")

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse("contact.list")


class ContactUpdateMailingView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactCreateUpdateMixin,
    BaseMixin,
    UpdateView,
):
    form_class = ContactMailingForm
    model = Contact

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": True})
        return kwargs

    def get_initial(self):
        result = super().get_initial()
        result.update(
            {
                "first_name": self.object.user.first_name,
                "last_name": self.object.user.last_name,
                "username": self.object.user.username,
                "email": self.object.email,
            }
        )
        return result

    def get_success_url(self):
        return reverse("contact.list")


class ContactUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactCreateUpdateMixin,
    BaseMixin,
    UpdateView,
):
    form_class = ContactForm
    model = Contact

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": True})
        return kwargs

    def get_initial(self):
        result = super().get_initial()
        result.update(
            {
                "first_name": self.object.user.first_name,
                "last_name": self.object.user.last_name,
                "username": self.object.user.username,
                "email": self.object.email,
                "phone": self.object.landline_phone,
                "mobile": self.object.mobile_phone,
            }
        )
        address = self.object.primary_address
        if address:
            result.update(
                {
                    "address_one": address.address_one,
                    "address_two": address.address_two,
                    "locality": address.locality,
                    "town": address.town,
                    "admin_area": address.admin_area,
                    "postal_code": address.postal_code,
                    "country": address.country,
                }
            )
        return result


class ContactDetailMixin(ContactPermMixin):
    model = Contact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        next_url = self.request.GET.get(REDIRECT_FIELD_NAME, "")
        context.update(self.object.get_multi_as_dict(), next=next_url)
        return context

    def test_contact(self):
        return self.get_object()


class ContactEmailCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraCreateMixin,
    BaseMixin,
    CreateView,
):
    form_class = ContactEmailForm
    model = ContactEmail

    def _all_tags(self):
        return ContactEmail.tags.all().order_by("name")


class ContactEmailDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraDeleteMixin,
    BaseMixin,
    UpdateView,
):
    model = ContactEmail
    form_class = ContactEmailEmptyForm
    template_name = "contact/contact_email_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())


class ContactEmailUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraUpdateMixin,
    BaseMixin,
    UpdateView,
):
    model = ContactEmail
    form_class = ContactEmailForm

    def _all_tags(self):
        return ContactEmail.tags.all().order_by("name")


class ContactListMixin:
    paginate_by = 20

    def get_queryset(self):
        qs = Contact.objects.current()
        return qs.order_by("user__username")


class ContactPhoneCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraCreateMixin,
    BaseMixin,
    CreateView,
):
    form_class = ContactPhoneForm
    model = ContactPhone

    def _all_tags(self):
        return ContactPhone.tags.all().order_by("name")


class ContactPhoneDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraDeleteMixin,
    BaseMixin,
    UpdateView,
):
    model = ContactPhone
    form_class = ContactPhoneEmptyForm
    template_name = "contact/contact_phone_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())


class ContactPhoneUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactExtraUpdateMixin,
    BaseMixin,
    UpdateView,
):
    model = ContactPhone
    form_class = ContactPhoneForm

    def _all_tags(self):
        return ContactPhone.tags.all().order_by("name")


class GenderListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    model = Gender
    paginate_by = 20


class GenderCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):

    form_class = GenderForm
    model = Gender

    def get_success_url(self):
        return reverse("contact.gender.list")


class GenderUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = GenderForm
    model = Gender

    def get_success_url(self):
        return reverse("contact.gender.list")
