# Generated by Django 2.0.3 on 2018-04-17 06:37

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [("contact", "0006_auto_20180417_0736")]

    operations = [
        migrations.RemoveField(model_name="contact", name="address_1"),
        migrations.RemoveField(model_name="contact", name="address_2"),
        migrations.RemoveField(model_name="contact", name="address_3"),
        migrations.RemoveField(model_name="contact", name="country"),
        migrations.RemoveField(model_name="contact", name="county"),
        migrations.RemoveField(model_name="contact", name="mobile"),
        migrations.RemoveField(model_name="contact", name="phone"),
        migrations.RemoveField(model_name="contact", name="postcode"),
        migrations.RemoveField(model_name="contact", name="slug"),
        migrations.RemoveField(model_name="contact", name="town"),
    ]
