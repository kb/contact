# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def _init_gender(model, slug, name):
    try:
        model.objects.get(slug=slug)
    except model.DoesNotExist:
        instance = model(**dict(slug=slug, name=name))
        instance.save()
        instance.full_clean()


def default_gender(apps, schema_editor):
    gender = apps.get_model("contact", "Gender")
    _init_gender(gender, "M", "Male")
    _init_gender(gender, "F", "Female")


class Migration(migrations.Migration):
    dependencies = [("contact", "0001_initial")]

    operations = [migrations.RunPython(default_gender)]
