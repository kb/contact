# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("contact", "0002_default_gender")]

    operations = [
        migrations.AddField(
            model_name="contact",
            name="subscribed",
            field=models.BooleanField(default=True),
        )
    ]
