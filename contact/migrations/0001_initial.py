# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [migrations.swappable_dependency(settings.AUTH_USER_MODEL)]

    operations = [
        migrations.CreateModel(
            name="Contact",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        serialize=False,
                        auto_created=True,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True)),
                ("company_name", models.CharField(max_length=100, blank=True)),
                (
                    "address_1",
                    models.CharField(
                        max_length=100, blank=True, verbose_name="Address"
                    ),
                ),
                (
                    "address_2",
                    models.CharField(
                        max_length=100, blank=True, verbose_name=""
                    ),
                ),
                (
                    "address_3",
                    models.CharField(
                        max_length=100, blank=True, verbose_name=""
                    ),
                ),
                ("town", models.CharField(max_length=100, blank=True)),
                ("county", models.CharField(max_length=100, blank=True)),
                ("postcode", models.CharField(max_length=20, blank=True)),
                ("country", models.CharField(max_length=100, blank=True)),
                ("phone", models.CharField(max_length=50, blank=True)),
                ("mobile", models.CharField(max_length=50, blank=True)),
                ("website", models.URLField(blank=True)),
                ("dob", models.DateField(blank=True, null=True)),
                ("position", models.CharField(max_length=50, blank=True)),
            ],
            options={
                "verbose_name_plural": "Contacts",
                "ordering": ("user__last_name", "user__first_name"),
                "verbose_name": "Contact",
            },
        ),
        migrations.CreateModel(
            name="Gender",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        serialize=False,
                        auto_created=True,
                        verbose_name="ID",
                    ),
                ),
                ("slug", models.CharField(max_length=3)),
                ("name", models.CharField(max_length=50)),
            ],
            options={
                "verbose_name_plural": "Genders",
                "ordering": ("slug",),
                "verbose_name": "Gender",
            },
        ),
        migrations.CreateModel(
            name="UserContact",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        serialize=False,
                        auto_created=True,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "contact",
                    models.ForeignKey(
                        to="contact.Contact", on_delete=models.CASCADE
                    ),
                ),
                (
                    "user",
                    models.OneToOneField(
                        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
            ],
            options={"abstract": False},
        ),
        migrations.AddField(
            model_name="contact",
            name="gender",
            field=models.ForeignKey(
                to="contact.Gender",
                blank=True,
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name="contact",
            name="user",
            field=models.OneToOneField(
                to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
        ),
    ]
