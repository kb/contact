# Generated by Django 2.2 on 2019-04-15 16:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("contact", "0011_auto_20190312_1526"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="usercontact",
            options={
                "ordering": ["user__username", "contact__user__username"],
                "verbose_name": "User Contact",
                "verbose_name_plural": "User Contacts",
            },
        ),
        migrations.AlterField(
            model_name="usercontact",
            name="user",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="usercontact", unique_together={("user", "contact")}
        ),
    ]
