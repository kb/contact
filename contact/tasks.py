# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.utils import timezone

from contact.search import ContactIndex
from search.search import SearchIndex


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def rebuild_contact_index():
    logger.info("Drop and rebuild contact index...")
    index = SearchIndex(ContactIndex())
    count = index.rebuild()
    logger.info("Contact index rebuilt - {} records - complete".format(count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def refresh_contact_index():
    logger.info("Refresh contact index...")
    index = SearchIndex(ContactIndex())
    count = index.refresh()
    logger.info("Contact index refreshed - {} records - complete".format(count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def update_contact_index(pk):
    logger.info("Search index - update contact {}".format(pk))
    index = SearchIndex(ContactIndex())
    count = index.update(pk)
    logger.info(
        "Search index - updated contact {} ({} record)".format(pk, count)
    )
    return count


def schedule_rebuild_contact_index():
    logger.info(
        ">>> schedule_rebuild_contact_index: {}".format(
            timezone.localtime(timezone.now()).strftime("%d/%m/%Y %H:%M")
        )
    )
    rebuild_contact_index.send()
