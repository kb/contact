# -*- encoding: utf-8 -*-
import pytest

from django.core.exceptions import PermissionDenied

from contact.models import check_perm_contact
from contact.tests.factories import ContactFactory, UserContactFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_check_perm_contact():
    user = UserFactory()
    contact = ContactFactory(user=user)
    assert check_perm_contact(user, contact) is True


@pytest.mark.django_db
def test_check_perm_contact_another_user():
    user = UserFactory(username="apple")
    contact = ContactFactory(user=UserFactory(username="orange"))
    with pytest.raises(PermissionDenied) as e:
        check_perm_contact(user, contact)
    assert "User 'apple' cannot access information for user 'orange'" in str(
        e.value
    )


@pytest.mark.django_db
def test_check_perm_contact_is_staff():
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=UserFactory())
    check_perm_contact(user, contact) is True


@pytest.mark.django_db
def test_check_perm_contact_user_contact():
    user = UserFactory(username="apple")
    contact = ContactFactory(user=UserFactory(username="orange"))
    UserContactFactory(user=user, contact=contact)
    assert check_perm_contact(user, contact) is True


@pytest.mark.django_db
def test_check_perm_contact_user_contact_not():
    user = UserFactory(username="apple")
    contact = ContactFactory(user=UserFactory(username="orange"))
    UserContactFactory(
        user=user, contact=ContactFactory(user=UserFactory(username="cherry"))
    )
    with pytest.raises(PermissionDenied) as e:
        check_perm_contact(user, contact)
    assert "User 'apple' cannot access information for user 'orange'" in str(
        e.value
    )
