# -*- encoding: utf-8 -*-
import pytest

from contact.models import Gender
from .factories import GenderFactory


@pytest.mark.django_db
def test_create_gender():
    Gender.objects.create_gender("M", "Male")


@pytest.mark.django_db
def test_factory():
    GenderFactory()


@pytest.mark.django_db
def test_female():
    obj = Gender.objects.female()
    assert not obj.male
    assert obj.female
    assert "F" == obj.slug


@pytest.mark.django_db
def test_male():
    obj = Gender.objects.male()
    assert obj.male
    assert not obj.female
    assert "M" == obj.slug


@pytest.mark.django_db
def test_male_does_not_exist():
    Gender.objects.male().delete()
    obj = Gender.objects.male()
    assert obj is None


@pytest.mark.django_db
def test_female_does_not_exist():
    Gender.objects.female().delete()
    obj = Gender.objects.female()
    assert obj is None


@pytest.mark.django_db
def test_str():
    assert "ABC" == str(GenderFactory(name="ABC"))
