# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus
from taggit.models import Tag

from contact.models import Contact, ContactAddress, ContactEmail, ContactPhone
from contact.tests.factories import (
    ContactAddressFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)
from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


def _setup_tags():
    """Create a contact and add tags, so they can be used by other contacts."""
    c = ContactFactory(user=UserFactory())
    ca = ContactAddressFactory(contact=c)
    ca.tags.add("A")
    ca.tags.add("B")
    ca.tags.add("C")
    ca.tags.add("Invoice")
    ce = ContactEmailFactory(contact=c)
    ce.tags.add("D")
    ce.tags.add("E")
    cp = ContactPhoneFactory(contact=c)
    cp.tags.add("F")
    cp.tags.add("G")


@pytest.mark.django_db
def test_contact_address_create(client):
    """Create an address address for a contact."""
    _setup_tags()
    tag_b = Tag.objects.get(name="B")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.address.create", args=[contact.pk])
    data = {
        "notes": "Orange",
        "address_one": "A",
        "address_two": "B",
        "locality": "C",
        "town": "D",
        "admin_area": "E",
        "postal_code": "EX20",
        "country": "UK",
        "tag": [tag_b.pk],
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_addresses()
    assert 1 == qs.count()
    contact_address = qs.first()
    assert "A, B, C, D, E, EX20, UK" == contact_address.address()
    assert "Orange" == contact_address.notes
    assert ["B"] == [x.name for x in contact_address.tags.all()]


@pytest.mark.django_db
def test_contact_address_create_get(client):
    """Create an address address for a contact."""
    _setup_tags()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.address.create", args=[contact.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert set(
        [
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
            "notes",
            "tag",
        ]
    ) == set(form.fields.keys())


@pytest.mark.django_db
def test_contact_address_create_simple(client):
    """Create an address address for a contact."""
    _setup_tags()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.address.create", args=[contact.pk, "Invoice"])
    data = {
        "notes": "Orange",
        "address_one": "A",
        "address_two": "B",
        "locality": "C",
        "town": "D",
        "admin_area": "E",
        "postal_code": "EX20",
        "country": "UK",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_addresses()
    assert 1 == qs.count()
    contact_address = qs.first()
    assert contact.primary_address is None
    assert "A, B, C, D, E, EX20, UK" == contact_address.address()
    assert "A, B, C, D, E, EX20, UK" == contact.invoice_address.address()
    assert "Orange" == contact_address.notes
    assert ["Invoice"] == [x.name for x in contact_address.tags.all()]


@pytest.mark.django_db
def test_contact_address_create_simple_get(client):
    """Create an address address for a contact."""
    _setup_tags()
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.address.create", args=[contact.pk, "Invoice"])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert set(
        [
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
            "notes",
        ]
    ) == set(form.fields.keys())


@pytest.mark.django_db
def test_contact_address_delete(client):
    """Delete an address address for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_address = ContactAddressFactory(contact=contact)
    url = reverse("contact.address.delete", args=[contact_address.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_addresses()
    assert 0 == qs.count()
    qs = ContactAddress.objects.all()
    assert 1 == qs.count()
    contact_address = ContactAddress.objects.first()
    assert contact_address.is_deleted is True


@pytest.mark.django_db
def test_contact_address_update(client):
    """Update an address address for a contact."""
    _setup_tags()
    tag_a = Tag.objects.get(name="A")
    tag_b = Tag.objects.get(name="B")
    tag_c = Tag.objects.get(name="C")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_address = ContactAddressFactory(contact=contact)
    contact_address.tags.add(tag_a)
    contact_address.tags.add(tag_c)
    assert set(["A", "C"]) == set([x.name for x in contact_address.tags.all()])
    url = reverse("contact.address.update", args=[contact_address.pk])
    data = {
        "notes": "Orange",
        "address_one": "A",
        "address_two": "B",
        "locality": "C",
        "town": "D",
        "admin_area": "E",
        "postal_code": "EX20",
        "country": "UK",
        "tag": [tag_b.pk],
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_addresses()
    assert 1 == qs.count()
    contact_address = qs.first()
    assert "A, B, C, D, E, EX20, UK" == contact_address.address()
    assert "Orange" == contact_address.notes
    assert ["B"] == [x.name for x in contact_address.tags.all()]


@pytest.mark.django_db
def test_contact_address_update_simple(client):
    """Update an address address for a contact."""
    _setup_tags()
    inv_tag = Tag.objects.get(name="Invoice")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_address = ContactAddressFactory(contact=contact)
    contact_address.tags.add(inv_tag)
    assert set([ContactAddress.INVOICE]) == set(
        [x.name for x in contact_address.tags.all()]
    )
    url = reverse("contact.address.update.simple", args=[contact_address.pk])
    data = {
        "notes": "Orange",
        "address_one": "A",
        "address_two": "B",
        "locality": "C",
        "town": "D",
        "admin_area": "E",
        "postal_code": "EX20",
        "country": "UK",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_addresses()
    assert 1 == qs.count()
    contact_address = qs.first()
    assert "A, B, C, D, E, EX20, UK" == contact_address.address()
    assert contact.primary_address is None
    assert "A, B, C, D, E, EX20, UK" == contact.invoice_address.address()
    assert "Orange" == contact_address.notes
    assert [ContactAddress.INVOICE] == [
        x.name for x in contact_address.tags.all()
    ]


@pytest.mark.django_db
def test_contact_address_update_initial(client):
    _setup_tags()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    tag_a = Tag.objects.get(name="A")
    tag_b = Tag.objects.get(name="B")
    tag_c = Tag.objects.get(name="C")
    inv_tag = Tag.objects.get(name="Invoice")
    # the contact we are testing
    contact = ContactFactory(user=UserFactory())
    contact_address = ContactAddressFactory(contact=contact)
    contact_address.tags.add("B")
    url = reverse("contact.address.update", args=[contact_address.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [tag_b.pk] == form["tag"].value()
    assert [tag_a.pk, tag_b.pk, tag_c.pk, inv_tag.pk] == [
        x.pk for x in form.fields["tag"].queryset
    ]


@pytest.mark.django_db
def test_contact_create(client):
    """Create a contact.

    .. note:: We don't add the email address to the user, we add it to the
              ``ContactEmail`` model.

    """
    u = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("contact.create")
    data = {
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "mobile": "07840",
        "phone": "01837",
        "username": "pkimber",
        "website": "http://www.kbsoftware.co.uk/",
        "notes": "Apple",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    contact = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert "" == contact.user.email
    assert "P" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert contact.user.has_usable_password() is False
    assert contact.user.is_active is False
    assert contact.user.is_staff is False
    assert "http://www.kbsoftware.co.uk/" == contact.website
    assert "Apple" == contact.notes
    assert contact.current_email().email == "test@pkimber.net"
    assert set(["01837", "07840"]) == set(
        [x.phone for x in contact.current_phones()]
    )
    assert 0 == contact.addresses.count()


@pytest.mark.django_db
def test_contact_create_mailing(client):
    """Create a contact for a mailing list."""
    consent = ConsentFactory(slug=Contact.GDPR_CONSENT_CONTACT_MAILING)
    user = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=user)
    assert [] == UserConsent.objects.user_consent_given(consent)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "username": "pkimber",
    }
    response = client.post(reverse("contact.create.mailing"), data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    contact = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.mailing.list") == response.url
    assert "" == contact.user.email
    assert "P" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert contact.user.has_usable_password() is False
    assert contact.user.is_active is False
    assert contact.user.is_staff is False
    assert "" == contact.website
    assert contact.current_email().email == "test@pkimber.net"
    assert "" == contact.mobile_phone
    assert "" == contact.landline_phone
    assert contact.invoice_address is None
    assert contact.primary_address is None
    assert 0 == contact.phones.count()
    assert 1 == contact.emails.count()
    contact_email = contact.emails.first()
    assert "test@pkimber.net" == contact_email.email
    assert [contact.user.pk] == UserConsent.objects.user_consent_given(consent)
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user == user_consent.user_updated


@pytest.mark.django_db
def test_contact_create_simple(client):
    """Create a contact.

    .. note:: We don't add the email address to the user, we add it to the
              ``ContactEmail`` model.

    """
    u = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "mobile": "07840",
        "phone": "01837",
        "username": "pkimber",
        "website": "http://www.kbsoftware.co.uk/",
        "address_one": "1 The Way",
        "address_two": "Waywards",
        "locality": "My Local",
        "town": "Townieville",
        "admin_area": "County",
        "postal_code": "EX30 9YZ",
        "country": "England",
    }
    response = client.post(reverse("contact.create"), data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    contact = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert "" == contact.user.email
    assert "P" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert contact.user.has_usable_password() is False
    assert contact.user.is_active is False
    assert contact.user.is_staff is False
    assert "http://www.kbsoftware.co.uk/" == contact.website
    assert contact.current_email().email == "test@pkimber.net"
    address = contact.primary_address
    assert address.address_one == "1 The Way"
    assert address.address_two == "Waywards"
    assert address.locality == "My Local"
    assert address.town == "Townieville"
    assert address.admin_area == "County"
    assert address.postal_code == "EX30 9YZ"
    assert address.country == "England"
    assert set([ContactAddress.PRIMARY]) == set(
        [x.name for x in address.tags.all()]
    )
    assert contact.mobile_phone == "07840"
    assert contact.landline_phone == "01837"
    assert contact.invoice_address is None
    assert contact.primary_address.address() == (
        "1 The Way, Waywards, My Local, Townieville, County, EX30 9YZ, England"
    )
    phones = {}
    for phone in contact.phones.all():
        phones[phone.phone] = set([x.name for x in phone.tags.all()])
    assert set(["01837", "07840"]) == phones.keys()
    assert set(["Landline"]) == phones["01837"]
    assert set(["Mobile"]) == phones["07840"]


@pytest.mark.django_db
def test_contact_create_no_company_or_last_name(client):
    """Create a contact.

    User must enter a company name or last name.

    """
    u = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {"first_name": "P", "username": "pkimber"}
    response = client.post(reverse("contact.create"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["Please enter a company name or last name"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_contact_create_company_name_only(client):
    """Create a contact.

    If the user enters a company name they don't need to enter a last name.

    """
    u = UserFactory(is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {"company_name": "KB", "username": "pkimber"}
    response = client.post(reverse("contact.create"), data)
    assert HTTPStatus.FOUND == response.status_code
    contact = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.detail", args=[contact.pk]) == response.url


@pytest.mark.django_db
def test_contact_create_last_name_only(client):
    """Create a contact.

    If the user enters a last name they don't need to enter a company name.

    """
    u = UserFactory(is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("contact.create")
    data = {"last_name": "Kimber", "username": "pkimber"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    contact = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.detail", args=[contact.pk]) == response.url


@pytest.mark.django_db
def test_contact_create_no_email(client):
    u = UserFactory(is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("contact.create")
    data = {"last_name": "Kimber", "username": "pkimber"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    obj = Contact.objects.get(user__username="pkimber")
    assert reverse("contact.detail", args=[obj.pk]) == response["Location"]
    assert "" == obj.user.email
    assert obj.user.is_staff is False


@pytest.mark.django_db
def test_contact_create_username_plus_space(client):
    u = UserFactory(is_staff=True)
    ContactFactory(user=u)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    url = reverse("contact.create")
    data = {"last_name": "Kimber", "username": "pkimber "}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "username": [
            (
                "This value may contain only letters, numbers "
                "and dot, dash and underscore."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_contact_create_username_already_exists(client):
    """Create a contact.

    The username already exists.

    """
    u = UserFactory(username="staff", is_staff=True)
    ContactFactory(user=u)
    ContactFactory(user=UserFactory(username="pkimber"))
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {"first_name": "P", "last_name": "Kimber", "username": "pkimber"}
    response = client.post(reverse("contact.create"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            (
                "Another contact already has the 'pkimber' username. "
                "Please try a different one."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_contact_delete(client):
    user_staff = UserFactory(is_staff=True)
    user = UserFactory()
    assert user.is_active is True
    assert user.has_usable_password() is True
    contact = ContactFactory(user=user)
    assert (
        client.login(username=user_staff.username, password=TEST_PASSWORD)
        is True
    )
    response = client.post(reverse("contact.delete", args=[contact.pk]))
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.list") == response.url
    contact.refresh_from_db()
    assert contact.is_deleted is True
    user.refresh_from_db()
    assert user.is_active is False
    assert user.has_usable_password() is False


@pytest.mark.django_db
def test_contact_email_create(client):
    """Create an email address for a contact."""
    _setup_tags()
    tag_e = Tag.objects.get(name="E")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.email.create", args=[contact.pk])
    data = {"email": "test@pkimber.net", "notes": "Orange", "tag": [tag_e.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_emails()
    assert 1 == qs.count()
    contact_email = qs.first()
    assert "test@pkimber.net" == contact_email.email
    assert "Orange" == contact_email.notes
    assert ["E"] == [x.name for x in contact_email.tags.all()]


@pytest.mark.django_db
def test_contact_email_delete(client):
    """Delete an email address for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_email = ContactEmailFactory(contact=contact)
    url = reverse("contact.email.delete", args=[contact_email.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_emails()
    assert 0 == qs.count()
    qs = ContactEmail.objects.all()
    assert 1 == qs.count()
    contact_email = ContactEmail.objects.first()
    assert contact_email.is_deleted is True


@pytest.mark.django_db
def test_contact_email_update(client):
    """Update an email address for a contact."""
    _setup_tags()
    tag_d = Tag.objects.get(name="D")
    tag_e = Tag.objects.get(name="E")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_email = ContactEmailFactory(contact=contact)
    contact_email.tags.add(tag_d)
    assert ["D"] == [x.name for x in contact_email.tags.all()]
    url = reverse("contact.email.update", args=[contact_email.pk])
    data = {"email": "test@pkimber.net", "notes": "Orange", "tag": [tag_e.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_emails()
    assert 1 == qs.count()
    contact_email = qs.first()
    assert "test@pkimber.net" == contact_email.email
    assert "Orange" == contact_email.notes
    assert ["E"] == [x.name for x in contact_email.tags.all()]


@pytest.mark.django_db
def test_contact_email_update_initial(client):
    _setup_tags()
    tag_d = Tag.objects.get(name="D")
    tag_e = Tag.objects.get(name="E")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # the contact we are testing
    contact = ContactFactory(user=UserFactory())
    contact_email = ContactEmailFactory(contact=contact)
    contact_email.tags.add("E")
    url = reverse("contact.email.update", args=[contact_email.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [tag_e.pk] == form["tag"].value()
    assert [tag_d.pk, tag_e.pk] == [x.pk for x in form.fields["tag"].queryset]


@pytest.mark.django_db
def test_contact_phone_create(client):
    """Create an phone address for a contact."""
    _setup_tags()
    tag_f = Tag.objects.get(name="F")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("contact.phone.create", args=[contact.pk])
    data = {"phone": "test@pkimber.net", "notes": "Orange", "tag": [tag_f.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_phones()
    assert 1 == qs.count()
    contact_phone = qs.first()
    assert "test@pkimber.net" == contact_phone.phone
    assert "Orange" == contact_phone.notes
    assert ["F"] == [x.name for x in contact_phone.tags.all()]


@pytest.mark.django_db
def test_contact_phone_delete(client):
    """Delete an phone address for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_phone = ContactPhoneFactory(contact=contact)
    url = reverse("contact.phone.delete", args=[contact_phone.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_phones()
    assert 0 == qs.count()
    qs = ContactPhone.objects.all()
    assert 1 == qs.count()
    contact_phone = ContactPhone.objects.first()
    assert contact_phone.is_deleted is True


@pytest.mark.django_db
def test_contact_phone_update(client):
    """Update an phone address for a contact."""
    _setup_tags()
    tag_f = Tag.objects.get(name="F")
    tag_g = Tag.objects.get(name="G")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    contact_phone = ContactPhoneFactory(contact=contact)
    contact_phone.tags.add(tag_f)
    assert ["F"] == [x.name for x in contact_phone.tags.all()]
    url = reverse("contact.phone.update", args=[contact_phone.pk])
    data = {"phone": "test@pkimber.net", "notes": "Orange", "tag": [tag_g.pk]}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    qs = contact.current_phones()
    assert 1 == qs.count()
    contact_phone = qs.first()
    assert "test@pkimber.net" == contact_phone.phone
    assert "Orange" == contact_phone.notes
    assert ["G"] == [x.name for x in contact_phone.tags.all()]


@pytest.mark.django_db
def test_contact_phone_update_initial(client):
    _setup_tags()
    tag_f = Tag.objects.get(name="F")
    tag_g = Tag.objects.get(name="G")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # the contact we are testing
    contact = ContactFactory(user=UserFactory())
    contact_phone = ContactPhoneFactory(contact=contact)
    contact_phone.tags.add("G")
    url = reverse("contact.phone.update", args=[contact_phone.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [tag_g.pk] == form["tag"].value()
    assert [tag_f.pk, tag_g.pk] == [x.pk for x in form.fields["tag"].queryset]


@pytest.mark.django_db
def test_contact_update(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="")
    contact = ContactFactory(user=user, notes="Orange")
    url = reverse("contact.update", args=[contact.pk])
    data = {
        "company_name": "KB",
        "first_name": "P",
        "last_name": "Kimber",
        "username": "pat",
        "website": "http://www.kbsoftware.co.uk/",
        "notes": "Apple",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    with pytest.raises(Contact.DoesNotExist):
        Contact.objects.get(user__username="pkimber")
    contact = Contact.objects.get(user__username="pat")
    assert "P" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert contact.user.is_staff is False
    assert "http://www.kbsoftware.co.uk/" == contact.website
    assert "Apple" == contact.notes
    assert 0 == contact.addresses.count()


@pytest.mark.django_db
def test_contact_update_initial(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(
        username="pkimber", email="", first_name="P", last_name="Kimber"
    )
    contact = ContactFactory(
        user=user, company_name="KB", website="http://www.kbsoftware.co.uk/"
    )
    url = reverse("contact.update", args=[contact.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "KB" == form["company_name"].value()
    assert "http://www.kbsoftware.co.uk/" == form["website"].value()
    assert "P" == form["first_name"].value()
    assert "Kimber" == form["last_name"].value()
    assert "pkimber" == form["username"].value()


@pytest.mark.django_db
def test_contact_update_mailing(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="")
    contact = ContactFactory(
        user=user, notes="Orange", website="http://kbsoftware.co.uk/"
    )
    url = reverse("contact.update.mailing", args=[contact.pk])
    data = {
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "username": "pat",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.list") == response.url
    with pytest.raises(Contact.DoesNotExist):
        Contact.objects.get(user__username="pkimber")
    contact = Contact.objects.get(user__username="pat")
    assert "P" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert contact.user.is_staff is False
    assert "http://kbsoftware.co.uk/" == contact.website
    assert contact.current_email().email == "test@pkimber.net"
    assert "Orange" == contact.notes
    assert 0 == contact.addresses.count()


@pytest.mark.django_db
def test_contact_update_mailing_initial(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(
        username="pkimber", email="", first_name="P", last_name="Kimber"
    )
    contact = ContactFactory(
        user=user, company_name="KB", website="http://www.kbsoftware.co.uk/"
    )
    ContactEmailFactory(contact=contact, email="code@pkimber.net")
    url = reverse("contact.update.mailing", args=[contact.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "KB" == form["company_name"].value()
    assert "code@pkimber.net" == form["email"].value()
    assert "P" == form["first_name"].value()
    assert "Kimber" == form["last_name"].value()
    assert "pkimber" == form["username"].value()


@pytest.mark.django_db
def test_contact_update_no_company_or_last_name(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="", last_name="")
    contact = ContactFactory(user=user, company_name="")
    url = reverse("contact.update", args=[contact.pk])
    data = {"first_name": "P", "username": "pat"}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["Please enter a company name or last name"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_contact_update_company_name_only(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="", last_name="")
    contact = ContactFactory(user=user, company_name="")
    url = reverse("contact.update", args=[contact.pk])
    data = {"company_name": "KB", "username": "pat"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("contact.detail", args=[contact.pk]) == response.url


@pytest.mark.django_db
def test_contact_update_last_name_only(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="", last_name="")
    contact = ContactFactory(user=user, company_name="")
    url = reverse("contact.update", args=[contact.pk])
    data = {"last_name": "Kimber", "username": "pat"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("contact.detail", args=[contact.pk]) == response.url


@pytest.mark.django_db
def test_contact_update_simple(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="")
    contact = ContactFactory(user=user, notes="Orange")
    address = ContactAddressFactory(contact=contact, address_one="Street")
    address.tags.add(ContactAddress.DELIVERY)
    address.tags.add(ContactAddress.PRIMARY)
    ContactEmailFactory(contact=contact, email="tester@pkimber.net")
    contact_pk = contact.pk
    data = {
        "username": "patrick",
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "mobile": "07840",
        "phone": "01837",
        "website": "http://www.kbsoftware.co.uk/",
        "address_one": "1 The Way",
        "address_two": "Waywards",
        "locality": "My Local",
        "town": "Townieville",
        "admin_area": "County",
        "postal_code": "EX30 9YZ",
        "country": "England",
        "notes": "Peach",
    }
    url = reverse("contact.update", args=[contact.pk])
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    contact = Contact.objects.get(user__username="patrick")
    assert contact_pk == contact.pk
    assert "P" == contact.user.first_name
    assert "test@pkimber.net" == contact.email()
    assert "Kimber" == contact.user.last_name
    assert contact.user.is_staff is False
    assert "http://www.kbsoftware.co.uk/" == contact.website
    assert "Peach" == contact.notes
    address = contact.primary_address
    assert address.address_one == "1 The Way"
    assert address.address_two == "Waywards"
    assert address.locality == "My Local"
    assert address.town == "Townieville"
    assert address.admin_area == "County"
    assert address.postal_code == "EX30 9YZ"
    assert address.country == "England"
    assert set([ContactAddress.DELIVERY, ContactAddress.PRIMARY]) == set(
        [x.name for x in address.tags.all()]
    )
    assert contact.invoice_address is None
    assert contact.primary_address.address() == (
        "1 The Way, Waywards, My Local, Townieville, County, EX30 9YZ, England"
    )
    assert contact.mobile_phone == "07840"
    assert contact.landline_phone == "01837"
    phones = {}
    for phone in contact.phones.all():
        phones[phone.phone] = set([x.name for x in phone.tags.all()])
    assert set(["01837", "07840"]) == phones.keys()
    assert set(["Landline"]) == phones["01837"]
    assert set(["Mobile"]) == phones["07840"]


@pytest.mark.django_db
def test_contact_update_simple_initially_incomplete(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="")
    contact = ContactFactory(user=user, notes="Orange")
    assert contact.primary_address is None
    assert contact.invoice_address is None
    assert contact.mobile_phone == ""
    assert contact.landline_phone == ""
    contact_pk = contact.pk
    data = {
        "username": "pkimber",
        "company_name": "KB",
        "email": "test@pkimber.net",
        "first_name": "P",
        "last_name": "Kimber",
        "mobile": "07840",
        "phone": "01837",
        "website": "http://www.kbsoftware.co.uk/",
        "address_one": "1 The Way",
        "address_two": "Waywards",
        "locality": "My Local",
        "town": "Townieville",
        "admin_area": "County",
        "postal_code": "EX30 9YZ",
        "country": "England",
        "notes": "Nectarine",
    }
    url = reverse("contact.update", args=[contact.pk])
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    contact = Contact.objects.get(user__username="pkimber")
    assert contact_pk == contact.pk
    assert "P" == contact.user.first_name
    assert "test@pkimber.net" == contact.email()
    assert "Kimber" == contact.user.last_name
    assert contact.user.is_staff is False
    assert "http://www.kbsoftware.co.uk/" == contact.website
    assert "Nectarine" == contact.notes
    add = contact.primary_address
    assert add.address_one == "1 The Way"
    assert add.address_two == "Waywards"
    assert add.locality == "My Local"
    assert add.town == "Townieville"
    assert add.admin_area == "County"
    assert add.postal_code == "EX30 9YZ"
    assert add.country == "England"
    assert contact.invoice_address is None
    assert contact.primary_address.address() == (
        "1 The Way, Waywards, My Local, Townieville, County, EX30 9YZ, England"
    )
    assert contact.mobile_phone == "07840"
    assert contact.landline_phone == "01837"
    assert set(["01837", "07840"]) == set(
        [x.phone for x in contact.current_phones()]
    )


@pytest.mark.django_db
def test_contact_update_username_already_exists(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    ContactFactory(user=UserFactory(username="pkimber"))
    user = UserFactory(username="pat", email="", last_name="")
    contact = ContactFactory(user=user, company_name="")
    url = reverse("contact.update", args=[contact.pk])
    data = {"last_name": "Kimber", "username": "pkimber"}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            (
                "Another contact already has the 'pkimber' username. "
                "Please try a different one."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_contact_update_username_no_change(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    user = UserFactory(username="pkimber", email="", last_name="")
    contact = ContactFactory(user=user, company_name="")
    url = reverse("contact.update", args=[contact.pk])
    data = {"last_name": "Kimber", "username": "pkimber"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
