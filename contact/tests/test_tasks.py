# -*- encoding: utf-8 -*-
import pytest

from contact.tasks import rebuild_contact_index, update_contact_index
from contact.tests.factories import ContactFactory


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_contact_index():
    ContactFactory()
    ContactFactory()
    assert 2 == rebuild_contact_index()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_update_contact_index():
    ContactFactory()
    contact = ContactFactory()
    assert 1 == update_contact_index(contact.pk)
