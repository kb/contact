# -*- encoding: utf-8 -*-
import pytest

from .factories import ContactFactory, GenderFactory


@pytest.mark.django_db
def test_pronoun_gender():
    contact = ContactFactory(
        gender=GenderFactory(
            pronoun_subject="he",
            pronoun_object="him",
            pronoun_dependent_possessive="his",
            pronoun_independent_possessive="his",
        )
    )
    assert "he" == contact.pronoun_subject()
    assert "him" == contact.pronoun_object()
    assert "his" == contact.pronoun_dependent_possessive()
    assert "his" == contact.pronoun_independent_possessive()


@pytest.mark.django_db
def test_pronoun_gender_blank():
    contact = ContactFactory(
        gender=GenderFactory(
            pronoun_subject="",
            pronoun_object="",
            pronoun_dependent_possessive="",
            pronoun_independent_possessive="",
        )
    )
    assert "they" == contact.pronoun_subject()
    assert "them" == contact.pronoun_object()
    assert "their" == contact.pronoun_dependent_possessive()
    assert "theirs" == contact.pronoun_independent_possessive()


@pytest.mark.django_db
def test_pronoun_gender_missing():
    contact = ContactFactory(gender=None)
    assert "they" == contact.pronoun_subject()
    assert "them" == contact.pronoun_object()
    assert "their" == contact.pronoun_dependent_possessive()
    assert "theirs" == contact.pronoun_independent_possessive()
