# -*- encoding: utf-8 -*-
import json
import pytest

from django.urls import reverse
from rest_framework import status

from api.tests.fixture import api_client
from base.url_utils import url_with_querystring
from contact.models import Contact
from contact.search import ContactIndex
from login.tests.factories import UserFactory
from search.search import SearchIndex
from .factories import (
    ContactAddressFactory,
    ContactConnectionFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)


@pytest.mark.django_db
def test_address_create(api_client):
    contact = ContactFactory()
    data = {
        "data": {
            "type": "contactAddresses",
            "id": f"{contact.pk}",
            "attributes": {
                "contact": contact.pk,
                "tags": ["Invoice", "Delivery"],
                "address_one": "a",
                "address_two": "b",
                "locality": "c",
                "town": "d",
                "admin_area": "e",
                "postal_code": "f",
                "country": "g",
                "notes": "Work Address",
            },
            "relationships": {
                "contact": {"data": {"type": "contact", "id": contact.pk}}
            },
        }
    }
    # from rich.pretty import pprint
    response = api_client.post(
        reverse("api:address-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    # pprint(data, expand_all=True)
    # print(data)
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = contact.current_addresses()
    assert 1 == qs.count()
    address = qs.first()
    assert contact == address.contact
    assert "a" == address.address_one
    assert "b" == address.address_two
    assert "c" == address.locality
    assert "d" == address.town
    assert "e" == address.admin_area
    assert "f" == address.postal_code
    assert "g" == address.country
    assert "Work Address" == address.notes
    assert ["Delivery", "Invoice"] == [
        x.name for x in address.tags.all().order_by("name")
    ]


@pytest.mark.django_db
def test_address_update(api_client):
    contact = ContactFactory()
    address = ContactAddressFactory(contact=contact)
    data = {
        "data": {
            "type": "contactAddresses",
            "id": f"{address.pk}",
            "attributes": {
                "tags": ["Invoice", "Delivery"],
                "address_one": "Number 11",
                "address_two": "Barn Farm",
                "locality": "Hatherleigh",
                "town": "Okehampton",
                "admin_area": "Devon",
                "postal_code": "EX32 7LD",
                "country": "UK",
                "notes": "Work Address",
            },
            "relationships": {
                "contact": {"data": {"type": "contact", "id": contact.pk}}
            },
        }
    }
    response = api_client.patch(
        reverse("api:address-detail", args=[address.pk]),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    address.refresh_from_db()
    assert contact == address.contact
    assert "Okehampton" == address.town
    assert "Number 11" == address.address_one
    assert "Barn Farm" == address.address_two
    assert "Hatherleigh" == address.locality
    assert "Devon" == address.admin_area
    assert "EX32 7LD" == address.postal_code
    assert "UK" == address.country
    assert "Work Address" == address.notes
    assert ["Delivery", "Invoice"] == [
        x.name for x in address.tags.all().order_by("name")
    ]


@pytest.mark.django_db
def test_addresses_update_remove_tags(api_client):
    contact = ContactFactory()
    address = ContactAddressFactory(contact=contact)
    address.tags.add("Delivery")
    address.tags.add("Home")
    address.tags.add("Invoice")
    assert ["Delivery", "Home", "Invoice"] == [
        x.name for x in address.tags.all().order_by("name")
    ]
    data = {
        "data": {
            "type": "contactAddresses",
            "id": f"{address.pk}",
            "attributes": {
                "tags": ["Delivery"],
                "address_one": "Number 11",
                "address_two": "Barn Farm",
                "locality": "Hatherleigh",
                "town": "Okehampton",
                "admin_area": "Devon",
                "postal_code": "EX32 7LD",
                "country": "UK",
            },
        }
    }
    response = api_client.patch(
        reverse("api:address-detail", args=[address.pk]),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    address.refresh_from_db()
    assert ["Delivery"] == [x.name for x in address.tags.all().order_by("name")]


@pytest.mark.django_db
def test_contact_x(api_client):
    user = UserFactory(
        email="web@pkimber.net",
        first_name="Patrick",
        last_name="Kimber",
        username="pkimber",
    )
    contact = ContactFactory(user=user, company_name="KB")
    connection_1 = ContactFactory()
    connection_2 = ContactFactory()
    contact_connection_1 = ContactConnectionFactory(
        contact=contact, connection=connection_1
    )
    contact_connection_2 = ContactConnectionFactory(
        contact=contact, connection=connection_2
    )
    contact_connection_2.set_deleted(user)
    email_1 = ContactEmailFactory(contact=contact, email="test@pkimber.net")
    email_1.set_deleted(user)
    email_2 = ContactEmailFactory(contact=contact, email="web@pkimber.net")
    phone_1 = ContactPhoneFactory(contact=contact, phone="00123 123 123")
    phone_2 = ContactPhoneFactory(contact=contact, phone="00567 567 567")
    phone_2.set_deleted(user)
    phone_3 = ContactPhoneFactory(contact=contact, phone="00890 890 890")
    response = api_client.get(
        reverse("api:contact-detail", args=[contact.pk]),
        content_type="application/vnd.api+json",
    )
    # from rich.pretty import pprint
    # pprint(response.json(), expand_all=True)
    assert status.HTTP_200_OK == response.status_code, response.data
    # print(json.dumps(response.json(), indent=4))
    assert {
        "data": {
            "type": "contact",
            "id": f"{contact.pk}",
            "attributes": {
                "company-name": "KB",
                "title": "",
                "last-name": "Kimber",
                "first-name": "Patrick",
                "notes": "",
                "addresses": [],
                "connections": [
                    {
                        "id": contact_connection_1.pk,
                        "contact": {"type": "contact", "id": f"{contact.pk}"},
                        "tags": [],
                        "connection": {
                            "type": "contact",
                            "id": f"{connection_1.pk}",
                        },
                        "notes": "",
                    }
                ],
                "phones": [
                    {
                        "id": phone_1.pk,
                        "contact": {"type": "contact", "id": f"{contact.pk}"},
                        "tags": [],
                        "phone": "00123 123 123",
                        "notes": "",
                    },
                    {
                        "id": phone_3.pk,
                        "contact": {"type": "contact", "id": f"{contact.pk}"},
                        "tags": [],
                        "phone": "00890 890 890",
                        "notes": "",
                    },
                ],
                "emails": [
                    {
                        "id": email_2.pk,
                        "contact": {"type": "contact", "id": f"{contact.pk}"},
                        "tags": [],
                        "email": "web@pkimber.net",
                        "notes": "",
                    }
                ],
            },
        }
    } == response.json()


@pytest.mark.django_db
def test_contact_create(api_client):
    data = {
        "data": {
            "type": "contact",
            "id": "1",
            "attributes": {
                "username": "pkimber",
                "first_name": "Patrick",
                "last_name": "Kimber",
                "company_name": "KB Software",
                "title": "Mr",
            },
        }
    }
    # from rich.pretty import pprint
    # pprint(data, expand_all=True)
    response = api_client.post(
        reverse("api:contact-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = Contact.objects.all()
    assert 1 == qs.count()
    contact = qs.first()
    assert "" == contact.user.email
    assert "patrick.kimber" == contact.user.username
    assert "Patrick" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert "Mr" == contact.title
    assert "KB Software" == contact.company_name


@pytest.mark.django_db
def test_contact_create_company_name_only(api_client):
    data = {
        "data": {
            "type": "contact",
            "id": "1",
            "attributes": {
                "company_name": "kb",
            },
        }
    }
    response = api_client.post(
        reverse("api:contact-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    # from rich.pretty import pprint
    # pprint(data, expand_all=True)
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = Contact.objects.all()
    assert 1 == qs.count()
    contact = qs.first()
    assert "" == contact.user.email
    assert "" == contact.user.first_name
    assert "" == contact.user.last_name
    assert "kb" == contact.user.username


@pytest.mark.django_db
def test_contact_create_no_data(api_client):
    """We auto-generate the username, so this is the same as passing no data."""
    data = {
        "data": {
            "type": "contact",
            "id": "1",
            "attributes": {
                "username": "pkimber",
            },
        }
    }
    response = api_client.post(
        reverse("api:contact-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    # from rich.pretty import pprint
    # pprint(data, expand_all=True)
    assert (
        status.HTTP_500_INTERNAL_SERVER_ERROR == response.status_code
    ), response.data
    qs = Contact.objects.all()
    assert 0 == qs.count()
    assert {
        "errors": [
            {
                "detail": "To create a unique user you must enter a company name, first name or last name",
                "status": "500",
                "source": {"pointer": "/data"},
                "code": "error",
            }
        ]
    } == response.json()


@pytest.mark.django_db
def test_contact_list(api_client):
    contact = ContactFactory(
        user=UserFactory(first_name="Laptop", last_name="Apple")
    )
    response = api_client.get(
        reverse("api:contact-list"), content_type="application/vnd.api+json"
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 1 == len(data)
    # from rich.pretty import pprint
    # pprint(data, expand_all=True)
    # print(data)
    assert data == {
        "data": [
            {
                "type": "contact",
                "id": f"{contact.pk}",
                "attributes": {
                    "company-name": "company_06",
                    "title": "",
                    "last-name": "Apple",
                    "first-name": "Laptop",
                    "notes": "",
                    "addresses": [],
                    "connections": [],
                    "phones": [],
                    "emails": [],
                },
            }
        ]
    }


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_contact_list_with_criteria(api_client):
    contact = ContactFactory(
        user=UserFactory(first_name="A", last_name="Orange"), company_name="KB"
    )
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 1 == index.update()
    url = url_with_querystring(reverse("api:contact-list"), criteria="orange")
    response = api_client.get(url, content_type="application/vnd.api+json")
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 1 == len(data)
    assert {
        "data": [
            {
                "type": "contact",
                "id": f"{contact.pk}",
                "attributes": {
                    "company-name": "KB",
                    "title": "",
                    "last-name": "Orange",
                    "first-name": "A",
                    "notes": "",
                    "addresses": [],
                    "connections": [],
                    "phones": [],
                    "emails": [],
                },
            }
        ]
    } == data


@pytest.mark.django_db
def test_contact_update(api_client):
    contact = ContactFactory(user=UserFactory(username="pkimber"))
    assert 1 == Contact.objects.all().count()
    data = {
        "data": {
            "type": "contact",
            "id": f"{contact.pk}",
            "attributes": {
                "username": "pkimber",
                "last_name": "Kimber",
                "first_name": "Patrick",
                "full_name": "P J D Kimber",
                "company_name": "KB Software",
                "title": "Mr",
            },
        }
    }
    response = api_client.patch(
        reverse("api:contact-detail", args=[contact.pk]),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    # from rich.pretty import pprint
    # pprint(data, expand_all=True)
    # print(data)
    assert status.HTTP_200_OK == response.status_code, response.data
    assert 1 == Contact.objects.all().count()
    contact.refresh_from_db()
    contact.user.refresh_from_db()
    assert "" == contact.user.email
    assert "pkimber" == contact.user.username
    assert "Patrick" == contact.user.first_name
    assert "Kimber" == contact.user.last_name
    assert "Mr" == contact.title
    assert "KB Software" == contact.company_name


@pytest.mark.django_db
def test_email_create(api_client):
    contact = ContactFactory()
    data = {
        "data": {
            "type": "contactEmails",
            "id": f"{contact.pk}",
            "attributes": {
                "tags": ["Home", "Work"],
                "email": "web@pkimber.net",
                "notes": "My home email address",
            },
            "relationships": {
                "contact": {"data": {"type": "contact", "id": contact.pk}}
            },
        }
    }
    response = api_client.post(
        reverse("api:email-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = contact.current_emails()
    assert 1 == qs.count()
    contact_email = qs.first()
    assert contact == contact_email.contact
    assert "web@pkimber.net" == contact_email.email
    assert ["Home", "Work"] == [
        x.name for x in contact_email.tags.all().order_by("name")
    ]


@pytest.mark.django_db
def test_phone_create(api_client):
    contact = ContactFactory()
    data = {
        "data": {
            "type": "contactPhones",
            "id": f"{contact.pk}",
            "attributes": {
                "tags": ["Landline", "Mobile"],
                "phone": "01234 678 890",
                "notes": "My home phone number",
            },
            "relationships": {
                "contact": {"data": {"type": "contact", "id": contact.pk}}
            },
        }
    }
    response = api_client.post(
        reverse("api:phone-list"),
        json.dumps(data),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = contact.current_phones()
    assert 1 == qs.count()
    contact_phone = qs.first()
    assert contact == contact_phone.contact
    assert "01234 678 890" == contact_phone.phone
    assert "My home phone number" == contact_phone.notes
    assert ["Landline", "Mobile"] == [
        x.name for x in contact_phone.tags.all().order_by("name")
    ]
