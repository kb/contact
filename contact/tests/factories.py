# -*- encoding: utf-8 -*-
import factory

from contact.models import (
    Contact,
    ContactAddress,
    ContactConnection,
    ContactEmail,
    ContactPhone,
    Gender,
    UserContact,
)
from login.tests.factories import UserFactory


class GenderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Gender


class ContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Contact

    gender = factory.SubFactory(GenderFactory)
    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def company_name(n):
        return "company_{:02d}".format(n)


class ContactAddressFactory(factory.django.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory)

    class Meta:
        model = ContactAddress

    @factory.sequence
    def address_one(n):
        return "address_one_{:02d}".format(n)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)

    @factory.sequence
    def town(n):
        return "town_{:02d}".format(n)


class ContactConnectionFactory(factory.django.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory)
    connection = factory.SubFactory(ContactFactory)

    class Meta:
        model = ContactConnection

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)


class ContactEmailFactory(factory.django.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory)

    class Meta:
        model = ContactEmail

    @factory.sequence
    def email(n):
        return "{:02d}@pkimber.net".format(n)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)


class ContactPhoneFactory(factory.django.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory)

    class Meta:
        model = ContactPhone

    @factory.sequence
    def phone(n):
        return "{:02d}@pkimber.net".format(n)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        if extracted:
            # A list of tags were passed in, use them.
            for tag in extracted:
                self.tags.add(tag)


class UserContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserContact

    contact = factory.SubFactory(ContactFactory)
    user = factory.SubFactory(UserFactory)
