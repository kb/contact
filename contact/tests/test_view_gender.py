# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.models import Gender
from contact.tests.factories import GenderFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_gender_create(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("contact.gender.create"),
        {
            "slug": "testing",
            "name": "Female",
            "pronoun_subject": "they",
            "pronoun_object": "them",
            "pronoun_dependent_possessive": "their",
            "pronoun_independent_possessive": "theirs",
        },
    )
    assert 3 == Gender.objects.count()
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert response.url == reverse("contact.gender.list")
    # assert 4 == Gender.objects.count()
    x = Gender.objects.get(slug="testing")
    assert "Female" == x.name
    assert "they" == x.pronoun_subject
    assert "them" == x.pronoun_object
    assert "their" == x.pronoun_dependent_possessive
    assert "theirs" == x.pronoun_independent_possessive


@pytest.mark.django_db
def test_gender_update(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    x = GenderFactory(
        slug="testing",
        name="Female",
        pronoun_subject="her",
        pronoun_object="hers",
        pronoun_dependent_possessive="their",
        pronoun_independent_possessive="theirs",
    )
    response = client.post(
        reverse("contact.gender.update", args=[x.pk]),
        {
            "slug": "testing",
            "name": "Male",
            "pronoun_subject": "they",
            "pronoun_object": "them",
            "pronoun_dependent_possessive": "their",
            "pronoun_independent_possessive": "theirs",
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert response.url == reverse("contact.gender.list")
    # assert 1 == Gender.objects.count()
    x.refresh_from_db()
    assert "testing" == x.slug
    assert "Male" == x.name
    assert "they" == x.pronoun_subject
    assert "them" == x.pronoun_object
    assert "their" == x.pronoun_dependent_possessive
    assert "theirs" == x.pronoun_independent_possessive
