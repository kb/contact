# -*- encoding: utf-8 -*-
import pytest

from contact.models import Contact, ContactAddress, ContactError, max_length
from login.tests.factories import GroupFactory, UserFactory
from search.tests.helper import check_search_methods
from .factories import (
    ContactAddressFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)


def _one_hundred_forty():
    return (
        "0123456789.123456789.123456789.123456789.123456789"
        "0123456789.123456789.123456789.123456789.123456789"
        "0123456789.123456789.123456789.123456789"
    )


@pytest.mark.django_db
def test_create_contact():
    user = UserFactory()
    x = Contact.objects.create_contact(user, company_name="KB", title="Mr")
    assert user == x.user
    assert "KB" == x.company_name
    assert "Mr" == x.title


@pytest.mark.django_db
def test_create_contact_no_kwargs():
    user = UserFactory()
    x = Contact.objects.create_contact(user)
    assert user == x.user
    assert "" == x.company_name
    assert "" == x.title


@pytest.mark.django_db
def test_deleted_contacts():
    ContactFactory(user=UserFactory(username="a"))
    contact = ContactFactory(user=UserFactory(username="b"))
    contact.set_deleted(UserFactory())
    ContactFactory(user=UserFactory(username="c"))
    contact = ContactFactory(user=UserFactory(username="d"))
    contact.set_deleted(UserFactory())
    assert ["b", "d"] == [
        obj.user.username for obj in Contact.objects.deleted_contacts()
    ]


@pytest.mark.django_db
def test_groups():
    contact = ContactFactory()
    group_1 = GroupFactory(name="g1")
    group_2 = GroupFactory(name="g2")
    contact.user.groups.add(group_1)
    contact.user.groups.add(group_2)
    assert ["g1", "g2"] == [x.name for x in contact.groups()]


@pytest.mark.django_db
def test_init_contact():
    user = UserFactory()
    x = Contact.objects.init_contact(user, company_name="KB", title="Mr")
    assert user == x.user
    assert "KB" == x.company_name
    assert "Mr" == x.title


@pytest.mark.django_db
def test_init_contact_already_exists():
    user = UserFactory()
    ContactFactory(user=user, company_name="", title="")
    x = Contact.objects.init_contact(user, company_name="KB", title="Mr")
    assert user == x.user
    assert "" == x.company_name
    assert "" == x.title


@pytest.mark.django_db
def test_is_delegate_for():
    contact = ContactFactory()
    assert contact.is_delegate_for(ContactFactory()) is False


@pytest.mark.parametrize(
    "x,expected",
    [
        ("abcde", "abc"),
        ("abcd", "abc"),
        ("abc", "abc"),
        ("ab", "ab"),
        ("a", "a"),
        ("", ""),
        (None, ""),
    ],
)
def test_max_length(x, expected):
    assert expected == max_length(x, 3)


# @pytest.mark.django_db
# def test_pks_with_email():
#    contact_1 = ContactFactory()
#    ContactEmailFactory(contact=contact_1, email="a@a.net")
#    contact_2 = ContactFactory()
#    ContactEmailFactory(contact=contact_2, email="b@b.net")
#    contact_3 = ContactFactory()
#    ContactEmailFactory(contact=contact_3, email="a@a.net")
#    contact_4 = ContactFactory()
#    ContactEmailFactory(contact=contact_4, email="a@a.net")
#    # test a second email address
#    ContactEmailFactory(contact=contact_4, email="b@b.net")
#    result = Contact.objects.pks_with_email(
#        "a@a.net", [contact_1.pk, contact_2.pk, contact_4.pk]
#    )
#    assert set([contact_1.pk, contact_2.pk, contact_4.pk]) == set(result)


@pytest.mark.django_db
def test_search_methods():
    contact = ContactFactory()
    check_search_methods(contact)


@pytest.mark.django_db
def test_str():
    contact = ContactFactory()
    str(contact)


@pytest.mark.django_db
def test_address():
    contact = ContactFactory()
    ContactAddressFactory(
        contact=contact,
        address_one="address_one",
        town="town",
        postal_code="EX20 2XL",
    )
    assert "address_one, town, EX20 2XL" == contact.address()


@pytest.mark.django_db
def test_address_br():
    contact = ContactFactory()
    ContactAddressFactory(
        contact=contact,
        address_one="address_one",
        town="town",
        postal_code="EX20 2XL",
    )
    assert "address_one<br>town<br>EX20 2XL" == contact.address("<br>")


@pytest.mark.django_db
def test_address_exclude_postcode():
    contact = ContactFactory()
    ContactAddressFactory(
        contact=contact,
        address_one="address_one",
        town="town",
        postal_code="EX20 2XL",
    )
    assert "address_one, town" == contact.address(exclude_postal_code=True)


@pytest.mark.django_db
def test_address_not():
    """If the contact does not have an address, return '' (not ``None``)."""
    contact = ContactFactory()
    assert "" == contact.address()


@pytest.mark.django_db
def test_address_tag():
    contact = ContactFactory()
    addr_1 = ContactAddressFactory(contact=contact, address_one="1", town="a")
    addr_2 = ContactAddressFactory(contact=contact, address_one="2", town="b")
    addr_3 = ContactAddressFactory(contact=contact, address_one="3", town="c")
    ContactAddressFactory(contact=contact, address_one="4", town="d")
    addr_1.tags.add(ContactAddress.INVOICE)
    addr_2.tags.add(ContactAddress.DELIVERY)
    addr_3.tags.add(ContactAddress.INVOICE)
    assert "3, c" == contact.address(tag=ContactAddress.INVOICE)


@pytest.mark.django_db
def test_contact_emails():
    ContactEmailFactory(email="a@a.com")
    ContactEmailFactory(email="b@b.com").set_deleted(UserFactory())
    ContactEmailFactory(email="c@c.com")
    assert ["a@a.com", "c@c.com"] == sorted(
        Contact.objects.contact_emails(
            ["a@a.com", "b@b.com", "c@c.com", "d@d.com"]
        )
    )


@pytest.mark.django_db
def test_create_unique_user():
    UserFactory(username="patrick.kimber")
    user = Contact.objects.create_unique_user("Patrick", "Kimber")
    assert "patrick.kimber.1" == user.username


@pytest.mark.django_db
def test_create_unique_user_first_last_name_too_long():
    one_hundred_forty = _one_hundred_forty()
    user = Contact.objects.create_unique_user(
        "pineapple.kiwi.{}".format(one_hundred_forty),
        "apple.orange.{}".format(one_hundred_forty),
    )
    assert Contact.MAX_LENGTH_FIRST_NAME == len(user.first_name)
    assert Contact.MAX_LENGTH_LAST_NAME == len(user.last_name)
    assert Contact.MAX_LENGTH_USERNAME == len(user.username)
    assert user.first_name.startswith("pineapple.kiwi.")
    assert user.last_name.startswith("apple.orange.")


@pytest.mark.django_db
def test_create_unique_user_long():
    user = Contact.objects.create_unique_user(
        "lemon.orange.walnut.green.blue", "patrick.apple.pineapple.kiwi.kimber"
    )
    assert (
        "lemon.orange.walnut.green.blue." "patrick.apple.pineapple.kiwi.kimber"
    ) == user.username


@pytest.mark.django_db
def test_create_unique_user_no_data():
    with pytest.raises(ContactError) as e:
        Contact.objects.create_unique_user(None, None)
    assert "you must enter a first name or last name" in str(e.value)


@pytest.mark.django_db
def test_create_unique_user_too_long_1():
    one_hundred_forty = _one_hundred_forty()
    user_name = max_length(
        "patrick.apple.pineapple.kimber.{}".format(one_hundred_forty),
        Contact.MAX_LENGTH_USERNAME,
    )
    UserFactory(username=user_name)
    user = Contact.objects.create_unique_user(
        "patrick.apple.{}".format(one_hundred_forty), "pineapple.kimber"
    )
    assert Contact.MAX_LENGTH_USERNAME == len(user.username)
    assert user.username.startswith("patrick.apple.")
    assert user.username.endswith(".pineapple.kimber")


@pytest.mark.django_db
def test_create_unique_user_too_long_2():
    one_hundred_forty = _one_hundred_forty()
    UserFactory(username="patrick.apple.pineapple.kimber")
    user = Contact.objects.create_unique_user(
        "", "patrick.apple.pineapple.kimber.{}".format(one_hundred_forty)
    )
    assert Contact.MAX_LENGTH_USERNAME == len(user.username)
    assert user.username.startswith("patrick.apple.pineapple.kimber.")


@pytest.mark.django_db
def test_create_unique_user_too_long_3():
    one_hundred_forty = _one_hundred_forty()
    user = Contact.objects.create_unique_user(
        "lemon.orange.walnut.green.blue.{}".format(one_hundred_forty),
        "patrick.apple.pineapple.kimber.{}".format(one_hundred_forty),
    )
    assert Contact.MAX_LENGTH_USERNAME == len(user.username)
    assert user.username.startswith("patrick.apple.pineapple.kimber")
    assert "lemon.orange.walnut.green.blue." not in user.username


@pytest.mark.django_db
def test_current():
    ContactFactory(company_name="a")
    ContactFactory(company_name="b").set_deleted(UserFactory())
    ContactFactory(company_name="c")
    assert ["a", "c"] == [
        x.company_name
        for x in Contact.objects.current().order_by("company_name")
    ]


@pytest.mark.parametrize(
    "company_name,title,first_name,last_name,expect",
    [
        ("", "Mr", "P", "Kimber", "Mr P Kimber"),
        ("", "", "P", "Kimber", "P Kimber"),
        ("", "", "", "Kimber", "Kimber"),
        ("", "", "", "", "patrick"),
        ("KB Software Ltd", "Mr", "P", "Kimber", "KB Software Ltd"),
    ],
)
@pytest.mark.django_db
def test_full_name(company_name, title, first_name, last_name, expect):
    user = UserFactory(
        first_name=first_name, last_name=last_name, username="patrick"
    )
    contact = ContactFactory(user=user, company_name=company_name, title=title)
    assert expect == contact.full_name
    # Used where the full name may be too long (same as ``full_name`` for now)
    assert expect == contact.get_short_name()


@pytest.mark.django_db
def test_get_multi_as_dict():
    contact = ContactFactory()
    ContactAddressFactory(contact=contact)
    ContactEmailFactory(contact=contact)
    ContactPhoneFactory(contact=contact)
    result = contact.get_multi_as_dict()
    assert set(
        ["contact_addresses", "contact_emails", "contact_phones"]
    ) == set(result.keys())


@pytest.mark.django_db
def test_current_addresses():
    """Get all the addresses for a contact."""
    contact = ContactFactory()
    ContactAddressFactory(
        contact=contact,
        address_one="address_a",
        town="town_a",
        postal_code="AA11 1AA",
        tags=[ContactAddress.DELIVERY, ContactAddress.INVOICE],
    )
    ContactAddressFactory(
        contact=contact,
        address_one="address_b",
        town="town_b",
        postal_code="BB22 2BB",
        tags=[ContactAddress.DELIVERY],
    )
    ContactAddressFactory(
        contact=contact,
        address_one="address_c",
        town="town_c",
        postal_code="CC33 3CC",
    )
    assert [
        "address_a, town_a, AA11 1AA",
        "address_b, town_b, BB22 2BB",
        "address_c, town_c, CC33 3CC",
    ] == [
        x.address() for x in contact.current_addresses().order_by("address_one")
    ]


@pytest.mark.django_db
def test_current_email():
    """Get all the addresses for a contact."""
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="3@test.com")
    ContactEmailFactory(contact=contact, email="2@test.com")
    contact_email = ContactEmailFactory(contact=contact, email="1@test.com")
    contact_email.set_deleted(UserFactory())
    current_email = contact.current_email()
    assert "2@test.com" == current_email.email


@pytest.mark.django_db
def test_current_emails():
    """Get all the addresses for a contact."""
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="3@test.com")
    contact_email = ContactEmailFactory(contact=contact, email="2@test.com")
    contact_email.set_deleted(UserFactory())
    ContactEmailFactory(contact=contact, email="1@test.com")
    assert ["1@test.com", "3@test.com"] == [
        x.email for x in contact.current_emails().order_by("email")
    ]


@pytest.mark.django_db
def test_email():
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="3@test.com")
    ContactEmailFactory(contact=contact, email="2@test.com")
    contact_email = ContactEmailFactory(contact=contact, email="1@test.com")
    contact_email.set_deleted(UserFactory())
    assert "2@test.com" == contact.email()


@pytest.mark.django_db
def test_email_none():
    """If we have no 'contact email' records, then get the user email."""
    contact = ContactFactory(user=UserFactory(email=""))
    assert "" == contact.email()


@pytest.mark.django_db
def test_email_user():
    """If we have no 'contact email' records, then get the user email."""
    contact = ContactFactory(user=UserFactory(email="user@test.com"))
    assert "user@test.com" == contact.email()


@pytest.mark.django_db
def test_matching_email():
    contact_1 = ContactFactory()
    ContactEmailFactory(contact=contact_1, email="code@pkimber.net")
    ContactEmailFactory(contact=contact_1, email="code@pkimber.net")
    contact_2 = ContactFactory()
    ContactEmailFactory(contact=contact_2, email="patrick@kbsoftware.co.uk")
    contact_3 = ContactFactory()
    contact_email = ContactEmailFactory(
        contact=contact_3, email="code@pkimber.net"
    )
    contact_email.set_deleted(UserFactory())
    contact_4 = ContactFactory()
    ContactEmailFactory(contact=contact_4, email="code@pkimber.net")
    contact_4.set_deleted(UserFactory())
    contact_5 = ContactFactory()
    ContactEmailFactory(contact=contact_5, email="code@pkimber.net")
    assert set([contact_1.pk, contact_5.pk]) == set(
        [x.pk for x in Contact.objects.matching_email("code@pkimber.net")]
    )


@pytest.mark.django_db
def test_phone():
    contact = ContactFactory()
    ContactPhoneFactory(contact=contact, phone="123")
    assert "123" == contact.phone()


@pytest.mark.django_db
def test_phone_does_not_exist():
    contact = ContactFactory()
    assert "" == contact.phone()


@pytest.mark.django_db
def test_set_deleted():
    user = UserFactory()
    assert user.is_active is True
    assert user.has_usable_password() is True
    contact = ContactFactory(user=user)
    contact.set_deleted(UserFactory())
    contact.refresh_from_db()
    assert contact.is_deleted is True
    user.refresh_from_db()
    assert user.is_active is False
    assert user.has_usable_password() is False


@pytest.mark.django_db
def test_undelete():
    user = UserFactory()
    assert user.is_active is True
    assert user.has_usable_password() is True
    contact = ContactFactory(user=user)
    contact.set_deleted(UserFactory())
    contact.refresh_from_db()
    assert contact.is_deleted is True
    contact.undelete()
    contact.refresh_from_db()
    assert contact.is_deleted is False
    user.refresh_from_db()
    assert user.is_active is True
    assert user.has_usable_password() is False


@pytest.mark.django_db
def test_user_in_a_group():
    """Is the user in the group?"""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    assert Contact.objects.user_in_a_group(user, [group.pk]) is True


@pytest.mark.django_db
def test_user_in_a_group_multi():
    """Is the user in the group?"""
    group_1 = GroupFactory()
    group_2 = GroupFactory()
    group_3 = GroupFactory()
    group_4 = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group_2)
    user.groups.add(group_4)
    assert Contact.objects.user_in_a_group(user, [group_1.pk]) is False
    assert Contact.objects.user_in_a_group(user, [group_2.pk]) is True
    assert Contact.objects.user_in_a_group(user, [group_3.pk]) is False
    assert Contact.objects.user_in_a_group(user, [group_4.pk]) is True
    assert (
        Contact.objects.user_in_a_group(user, [group_1.pk, group_2.pk]) is True
    )
    assert (
        Contact.objects.user_in_a_group(user, [group_1.pk, group_3.pk]) is False
    )
    assert (
        Contact.objects.user_in_a_group(user, [group_2.pk, group_4.pk]) is True
    )
    assert (
        Contact.objects.user_in_a_group(user, [group_3.pk, group_4.pk]) is True
    )
    assert (
        Contact.objects.user_in_a_group(
            user, [group_1.pk, group_2.pk, group_3.pk, group_4.pk]
        )
        is True
    )


@pytest.mark.django_db
def test_user_in_a_group_not():
    """Is the user in the group?"""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    group_with_no_user = GroupFactory()
    assert (
        Contact.objects.user_in_a_group(user, [group_with_no_user.pk]) is False
    )


@pytest.mark.django_db
def test_workflow_groups():
    contact = ContactFactory()
    group_1 = GroupFactory(name="g1")
    group_2 = GroupFactory(name="g2")
    group_3 = GroupFactory(name="g3")
    contact.user.groups.add(group_1)
    contact.user.groups.add(group_3)
    assert {
        group_1.pk: "g1",
        group_3.pk: "g3",
    } == contact.workflow_groups()
