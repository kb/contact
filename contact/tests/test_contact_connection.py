# -*- encoding: utf-8 -*-
import pytest

from contact.models import ContactConnection
from login.tests.factories import UserFactory
from .factories import ContactConnectionFactory, ContactFactory


def _connections():
    contact = ContactFactory(user=UserFactory())
    user_1 = UserFactory(first_name="1", last_name="A")
    user_2 = UserFactory(first_name="2", last_name="B")
    user_3 = UserFactory(first_name="3", last_name="C")
    for user in (user_1, user_2, user_3):
        ContactConnectionFactory(
            contact=contact, connection=ContactFactory(user=user)
        )
    connection = ContactConnection.objects.get(connection__user=user_2)
    connection.set_deleted(contact.user)


@pytest.mark.django_db
def test_current_manager_all():
    _connections()
    qs = ContactConnection.objects.current().order_by(
        "connection__user__first_name"
    )
    assert ["1", "3"] == [x.connection.user.first_name for x in qs]


@pytest.mark.django_db
def test_objects_manager_all():
    _connections()
    qs = ContactConnection.objects.all().order_by(
        "connection__user__first_name"
    )
    assert ["1", "2", "3"] == [x.connection.user.first_name for x in qs]


@pytest.mark.django_db
def test_objects_manager_current():
    _connections()
    qs = ContactConnection.objects.current().order_by(
        "connection__user__first_name"
    )
    assert ["1", "3"] == [x.connection.user.first_name for x in qs]


@pytest.mark.django_db
def test_str():
    user_1 = UserFactory(first_name="B", last_name="Penny")
    user_2 = UserFactory(first_name="P", last_name="Kimber")
    contact_connection = ContactConnectionFactory(
        contact=ContactFactory(user=user_1, company_name=""),
        connection=ContactFactory(user=user_2, company_name=""),
    )
    assert "B Penny to P Kimber" == str(contact_connection)
