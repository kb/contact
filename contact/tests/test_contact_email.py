# -*- encoding: utf-8 -*-
import pytest

from contact.models import ContactEmail
from login.tests.factories import UserFactory
from .factories import ContactEmailFactory, ContactFactory


def _emails():
    contact = ContactFactory(user=UserFactory())
    for x in ("1", "2", "3"):
        ContactEmailFactory(
            contact=contact, email="web{}@pkimber.net".format(x)
        )
    email = ContactEmail.objects.get(email="web2@pkimber.net")
    email.set_deleted(contact.user)


@pytest.mark.django_db
def test_contact_emails():
    ContactEmailFactory(email="a@a.com")
    ContactEmailFactory(email="b@b.com").set_deleted(UserFactory())
    ContactEmailFactory(email="c@c.com")
    assert set(["a@a.com", "c@c.com"]) == set(
        ContactEmail.objects.contact_emails(
            ["a@a.com", "b@b.com", "c@c.com", "d@d.com"]
        )
    )


@pytest.mark.django_db
def test_create_contact_email():
    contact = ContactFactory()
    x = ContactEmail.objects.create_contact_email(contact, "web@pkimber.net")
    assert contact == x.contact
    assert "web@pkimber.net" == x.email


@pytest.mark.django_db
def test_current_manager_all():
    _emails()
    qs = ContactEmail.objects.current().order_by("email")
    assert ["web1@pkimber.net", "web3@pkimber.net"] == [x.email for x in qs]


@pytest.mark.django_db
def test_objects_manager_all():
    _emails()
    qs = ContactEmail.objects.all().order_by("email")
    assert ["web1@pkimber.net", "web2@pkimber.net", "web3@pkimber.net"] == [
        x.email for x in qs
    ]


@pytest.mark.django_db
def test_objects_manager_current():
    _emails()
    qs = ContactEmail.objects.current().order_by("email")
    assert ["web1@pkimber.net", "web3@pkimber.net"] == [x.email for x in qs]


@pytest.mark.django_db
def test_tag_names():
    email = ContactEmailFactory(contact=ContactFactory())
    email.tags.add("Home", "Work")
    assert "(Home, Work)" == email.tag_names()


@pytest.mark.django_db
def test_tags():
    email = ContactEmailFactory(contact=ContactFactory())
    email.tags.add("Home", "Work")
    email.refresh_from_db()
    assert ["Home", "Work"] == [
        x.name for x in email.tags.all().order_by("name")
    ]
