# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.models import ContactAddress
from contact.tests.factories import (
    ContactFactory,
    ContactAddressFactory,
    ContactEmailFactory,
    ContactPhoneFactory,
    GenderFactory,
    UserContactFactory,
)
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_contact_address_create(perm_check):
    contact = ContactFactory()
    url = reverse("contact.address.create", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_address_simple_create(perm_check):
    contact = ContactFactory()
    url = reverse(
        "contact.address.create", args=[contact.pk, ContactAddress.INVOICE]
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_address_simple_update(perm_check):
    contact_address = ContactAddressFactory()
    url = reverse("contact.address.update.simple", args=[contact_address.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_address_delete(perm_check):
    contact_address = ContactAddressFactory()
    url = reverse("contact.address.delete", args=[contact_address.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_address_update(perm_check):
    contact_address = ContactAddressFactory()
    url = reverse("contact.address.update", args=[contact_address.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_create(perm_check):
    url = reverse("contact.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_create_simple(perm_check):
    url = reverse("contact.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_delete(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.update", kwargs={"pk": user_contact.contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_email_create(perm_check):
    contact = ContactFactory()
    url = reverse("contact.email.create", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_email_delete(perm_check):
    contact_email = ContactEmailFactory()
    url = reverse("contact.email.delete", args=[contact_email.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_email_update(perm_check):
    contact_email = ContactEmailFactory()
    url = reverse("contact.email.update", args=[contact_email.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_phone_create(perm_check):
    contact = ContactFactory()
    url = reverse("contact.phone.create", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_phone_delete(perm_check):
    contact_phone = ContactPhoneFactory()
    url = reverse("contact.phone.delete", args=[contact_phone.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_phone_update(perm_check):
    contact_phone = ContactPhoneFactory()
    url = reverse("contact.phone.update", args=[contact_phone.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_update(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.update", kwargs={"pk": user_contact.contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_update_simple(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.update", kwargs={"pk": user_contact.contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_gender_create(perm_check):
    url = reverse("contact.gender.create")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_gender_list(perm_check):
    GenderFactory(slug="slug-1")
    GenderFactory(slug="slug-2")
    url = reverse("contact.gender.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_gender_update(perm_check):
    gender = GenderFactory(slug="female")
    url = reverse("contact.gender.update", args=[gender.pk])
    perm_check.superuser(url)
