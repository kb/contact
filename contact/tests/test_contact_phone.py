# -*- encoding: utf-8 -*-
import pytest

from contact.models import ContactPhone
from login.tests.factories import UserFactory
from .factories import ContactFactory, ContactPhoneFactory


def _phones():
    contact = ContactFactory(user=UserFactory())
    for phone in ("1", "2", "3"):
        ContactPhoneFactory(contact=contact, phone=phone)
    phone = ContactPhone.objects.get(phone="2")
    phone.set_deleted(contact.user)


@pytest.mark.django_db
def test_current_manager_all():
    _phones()
    qs = ContactPhone.objects.current().order_by("phone")
    assert ["1", "3"] == [x.phone for x in qs]


@pytest.mark.django_db
def test_objects_manager_all():
    _phones()
    qs = ContactPhone.objects.all().order_by("phone")
    assert ["1", "2", "3"] == [x.phone for x in qs]


@pytest.mark.django_db
def test_objects_manager_current():
    _phones()
    qs = ContactPhone.objects.current().order_by("phone")
    assert ["1", "3"] == [x.phone for x in qs]


@pytest.mark.django_db
def test_str():
    contact_phone = ContactPhoneFactory(
        contact=ContactFactory(
            user=UserFactory(first_name="P", last_name="K"), company_name=""
        ),
        phone="01363 363 363",
    )
    assert "P K 01363 363 363" == str(contact_phone)


@pytest.mark.django_db
def test_tag_names():
    phone = ContactPhoneFactory(contact=ContactFactory())
    phone.tags.add("Mobile", "Work")
    assert "(Mobile, Work)" == phone.tag_names()


@pytest.mark.django_db
def test_tags():
    phone = ContactPhoneFactory(contact=ContactFactory())
    phone.tags.add("Mobile", "Work")
    phone.refresh_from_db()
    assert ["Mobile", "Work"] == [
        x.name for x in phone.tags.all().order_by("name")
    ]
