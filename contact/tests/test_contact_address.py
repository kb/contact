# -*- encoding: utf-8 -*-
import pytest

from contact.models import ContactAddress
from login.tests.factories import UserFactory
from .factories import ContactAddressFactory, ContactFactory


def _addresses():
    contact = ContactFactory(user=UserFactory())
    for x in ("1", "2", "3"):
        ContactAddressFactory(contact=contact, address_one=x)
    address = ContactAddress.objects.get(address_one="2")
    address.set_deleted(contact.user)


@pytest.mark.django_db
def test_address():
    contact = ContactFactory(user=UserFactory())
    address = ContactAddressFactory(
        contact=contact,
        address_one="Orange",
        address_two="Farm",
        locality="Zeal",
        town="Crediton",
        admin_area="Devon",
        postal_code="EX17",
        country="UK",
    )
    assert "Orange, Farm, Zeal, Crediton, Devon, EX17, UK" == address.address()


@pytest.mark.django_db
def test_address_not():
    """If the address is empty, return '' (not ``None``)."""
    contact = ContactFactory(user=UserFactory())
    address = ContactAddressFactory(
        contact=contact,
        address_one="",
        address_two="",
        locality="",
        town="",
        admin_area="",
        postal_code="",
        country="",
    )
    assert "" == address.address()


@pytest.mark.django_db
def test_address_with_newline():
    """For use in a template."""
    contact = ContactFactory(user=UserFactory())
    address = ContactAddressFactory(
        contact=contact,
        address_one="Orange",
        address_two="Farm",
        locality="Zeal",
        town="Crediton",
        admin_area="Devon",
        postal_code="EX17",
        country="UK",
    )
    assert (
        "Orange\nFarm\nZeal\nCrediton\nDevon\nEX17\nUK"
        == address.address_with_newline()
    )


@pytest.mark.django_db
def test_current_manager_all():
    _addresses()
    qs = ContactAddress.objects.current().order_by("address_one")
    assert ["1", "3"] == [x.address_one for x in qs]


@pytest.mark.django_db
def test_objects_manager_all():
    _addresses()
    qs = ContactAddress.objects.all().order_by("address_one")
    assert ["1", "2", "3"] == [x.address_one for x in qs]


@pytest.mark.django_db
def test_objects_manager_current():
    _addresses()
    qs = ContactAddress.objects.current().order_by("address_one")
    assert ["1", "3"] == [x.address_one for x in qs]


@pytest.mark.django_db
def test_tag_names():
    address = ContactAddressFactory(contact=ContactFactory())
    address.tags.add("Invoice", "Delivery")
    assert "(Delivery, Invoice)" == address.tag_names()


@pytest.mark.django_db
def test_tags():
    address = ContactAddressFactory(contact=ContactFactory())
    address.tags.add("Invoice", "Delivery")
    address.save()
    address.refresh_from_db()
    assert ["Delivery", "Invoice"] == [
        x.name for x in address.tags.all().order_by("name")
    ]
