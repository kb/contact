# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from contact.models import UserContact
from login.tests.factories import UserFactory
from .factories import ContactFactory, UserContactFactory


@pytest.mark.django_db
def test_for_user():
    user = UserFactory(username="a")
    UserContactFactory(
        user=user, contact=ContactFactory(user=UserFactory(username="1"))
    )
    UserContactFactory(
        user=UserFactory(),
        contact=ContactFactory(user=UserFactory(username="2")),
    )
    UserContactFactory(
        user=user, contact=ContactFactory(user=UserFactory(username="3"))
    )
    contact = ContactFactory(user=UserFactory(username="4"))
    contact.set_deleted(user)
    UserContactFactory(user=UserFactory(), contact=contact)
    assert ["1", "3"] == [
        x.contact.user.username for x in UserContact.objects.for_user(user)
    ]


@pytest.mark.django_db
def test_create_user_contact_multi():
    """Make sure a user can only link to more than one contact"""
    user = UserFactory(username="pat")
    UserContact.objects.create_user_contact(user, ContactFactory())
    UserContact.objects.create_user_contact(user, ContactFactory())
    assert 2 == UserContact.objects.for_user(user).count()


@pytest.mark.django_db
def test_create_user_contact_no_duplicate():
    user = UserFactory(username="pat")
    contact = ContactFactory()
    UserContact.objects.create_user_contact(user, contact)
    with pytest.raises(IntegrityError) as e:
        UserContact.objects.create_user_contact(user, contact)
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_ordering():
    user = UserFactory(username="a")
    UserContactFactory(
        user=user, contact=ContactFactory(user=UserFactory(username="3"))
    )
    UserContactFactory(
        user=UserFactory(),
        contact=ContactFactory(user=UserFactory(username="2")),
    )
    UserContactFactory(
        user=user, contact=ContactFactory(user=UserFactory(username="1"))
    )
    assert ["1", "3"] == [
        x.contact.user.username for x in UserContact.objects.for_user(user)
    ]


@pytest.mark.django_db
def test_str():
    user = UserFactory(username="pat")
    user_contact = UserContact.objects.create_user_contact(
        user,
        ContactFactory(user=UserFactory(first_name="andrea"), company_name=""),
    )
    assert "pat - andrea" == str(user_contact)
