# -*- encoding: utf-8 -*-
import json
import pytest

from contact.models import ContactAddress
from contact.search import ContactIndex, ContactIndexMixin, Row
from contact.tests.factories import (
    ContactAddressFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)
from login.tests.factories import UserFactory
from search.models import SearchFormat
from search.search import SearchIndex


class ContactSoftDeleteIndex(ContactIndexMixin):
    @property
    def is_soft_delete(self):
        return True


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_data_as_attr():
    contact = ContactFactory(
        company_name="KB", user=UserFactory(first_name="P", last_name="Kimber")
    )
    ContactAddressFactory(
        contact=contact,
        address_one="1 Market Street",
        town="Hatherleigh",
        postal_code="EX20 3CD",
    )
    ContactEmailFactory(contact=contact, email="web@pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 1 == index.update()
    result, total = index.search("EX20", data_format=SearchFormat.ATTR)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]
    assert [
        Row(
            address="1 Market Street, Hatherleigh",
            email="web@pkimber.net",
            name="KB: P Kimber",
            postcode="EX20 3CD",
        )
    ] == [x.data for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_drop_create():
    search_index = SearchIndex(ContactIndex())
    search_index.drop_create()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search():
    c1 = ContactFactory(
        company_name="Applewood",
        user=UserFactory(first_name="Phil", last_name="Part", email="p@p.com"),
    )
    ContactAddressFactory(
        contact=c1, address_one="Path", town="Poole", postal_code="EX2 05B"
    )
    c2 = ContactFactory(
        company_name="Zero",
        user=UserFactory(first_name="P", last_name="Kimber", email="a@a.com"),
    )
    ContactEmailFactory(contact=c2, email="a@a.com")
    ContactAddressFactory(
        contact=c2, address_one="Apple", town="Apple", postal_code="EX2 2AB"
    )
    u3 = UserFactory(first_name="Orange", last_name="Apple")
    c3 = ContactFactory(company_name="Crab Apple", user=u3)
    ContactEmailFactory(contact=c3, email="c@c.com")
    ContactAddressFactory(
        contact=c3,
        tags=[ContactAddress.DELIVERY],
        address_one="Carrot",
        town="Carrot",
        postal_code="TQ20 2AB",
    )
    u4 = UserFactory(first_name="Orange", last_name="Banana", email="b@b.com")
    c4 = ContactFactory(company_name="Tree", user=u4)
    ContactEmailFactory(contact=c4, email="b@b.com")
    ContactAddressFactory(
        contact=c4,
        tags=[ContactAddress.INVOICE],
        address_one="Potato",
        address_two="Apple",
        locality="Apple",
        town="Apple",
        admin_area="Apple",
        postal_code="TQ20 2AB",
    )
    search_index = SearchIndex(ContactIndex())
    search_index.drop_create()
    assert 4 == search_index.update()
    result, total = search_index.search(
        "apple", data_format=SearchFormat.COLUMN
    )
    assert 4 == total
    assert [c3.pk, c2.pk, c4.pk, c1.pk] == [x.pk for x in result]
    check = []
    for row in result:
        data = row.data
        col_list = []
        for column in data:
            line_list = []
            for line in column:
                line_list.append(line.text)
            col_list.append(line_list)
        check.append(col_list)
    assert [
        [
            ["Crab Apple: Orange Apple"],
            ["Carrot, Carrot TQ20 2AB"],
            ["c@c.com"],
        ],
        [["Zero: P Kimber"], ["Apple, Apple EX2 2AB"], ["a@a.com"]],
        [
            ["Tree: Orange Banana"],
            ["Potato, Apple, Apple, Apple, Apple TQ20 2AB"],
            ["b@b.com"],
        ],
        [["Applewood: Phil Part"], ["Path, Poole EX2 05B"], [""]],
    ] == check


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_company_name():
    ContactFactory(company_name="Apple", user=UserFactory())
    contact = ContactFactory(company_name="Orange", user=UserFactory())
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("orange", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_deleted_hard_delete():
    """The ``ContactIndex`` will actually delete documents."""
    user = UserFactory()
    c1 = ContactFactory(company_name="KB")
    ContactAddressFactory(contact=c1, postal_code="EX20 3CD")
    c2 = ContactFactory(company_name="Connect")
    ContactAddressFactory(contact=c2, postal_code="EX20 2AB")
    c3 = ContactFactory(company_name="Apple")
    ContactAddressFactory(contact=c3, postal_code="EX20 05B")
    c4 = ContactFactory(company_name="Apple Cake")
    ContactAddressFactory(contact=c4, postal_code="EX2 2AB")
    c2.set_deleted(user)
    c4.set_deleted(user)
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    # exclude deleted
    result, total = index.search("EX2", data_format=SearchFormat.COLUMN)
    assert 2 == total
    assert [c3.pk, c1.pk] == [x.pk for x in result]
    # include deleted - should be ignored!!
    result, total = index.search(
        "EX2", include_deleted=True, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [c3.pk, c1.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_deleted_soft_delete():
    """The ``ContactSoftDeleteIndex`` will just mark documents as deleted."""
    user = UserFactory()
    c1 = ContactFactory(company_name="KB")
    ContactAddressFactory(contact=c1, postal_code="EX20 3CD")
    c2 = ContactFactory(company_name="Connect")
    ContactAddressFactory(contact=c2, postal_code="EX20 2AB")
    c3 = ContactFactory(company_name="Apple")
    ContactAddressFactory(contact=c3, postal_code="EX2 05B")
    c4 = ContactFactory(company_name="Apple Cake")
    ContactAddressFactory(contact=c4, postal_code="EX2 2AB")
    c2.set_deleted(user)
    c4.set_deleted(user)
    index = SearchIndex(ContactSoftDeleteIndex())
    index.drop_create()
    assert 4 == index.update()
    # exclude deleted
    result, total = index.search("EX2", data_format=SearchFormat.COLUMN)
    assert 2 == total
    assert [c3.pk, c1.pk] == [x.pk for x in result]
    # include deleted
    result, total = index.search(
        "EX2", include_deleted=True, data_format=SearchFormat.COLUMN
    )
    assert 4 == total
    assert [c3.pk, c4.pk, c2.pk, c1.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_email():
    ContactFactory(user=UserFactory(email="patrick@kbsoftware.co.uk"))
    contact_1 = ContactFactory(user=UserFactory(email=""))
    ContactEmailFactory(contact=contact_1, email="web@pkimber.net")
    # email address does not index correctly unless lower case!?
    # https://www.kbsoftware.co.uk/crm/ticket/6155/
    contact_2 = ContactFactory(user=UserFactory(email=""))
    ContactEmailFactory(contact=contact_2, email="Web@Pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 3 == index.update()
    result, total = index.search(
        "web@pkimber.net", data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [contact_1.pk, contact_2.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
@pytest.mark.parametrize(
    "email", ["patrick@kbsoftware.co.uk", "web@pkimber.net"]
)
def test_search_email_partial_what_does_it_do(email):
    """A test to show how the email address is tokenized.

    email search, review and understand "max_token_length" with "uax_url_email":
    https://www.kbsoftware.co.uk/crm/ticket/3722/

    """
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 0 == index.update()
    print(index.analyze("email", email))


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_email_username():
    """Searching for ``sharon@gmail.com`` should match the second contact.

    The original code was matching the first contact, because the username was
    also ``sharon``.  To solve this issue we lowered the ``boost`` on the
    ``username`` field.

    """
    c1 = ContactFactory(
        user=UserFactory(
            username="sharon", first_name="Sharon", last_name="Apple"
        )
    )
    c2 = ContactFactory(
        user=UserFactory(
            username="pat", first_name="Sharon", last_name="Orange", email=""
        )
    )
    ContactEmailFactory(contact=c2, email="sharon@gmail.com")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search(
        "sharon@gmail.com", data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [c2.pk, c1.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_phone():
    ContactFactory(user=UserFactory())
    contact = ContactFactory(user=UserFactory())
    ContactPhoneFactory(contact=contact, phone="01234 123 123")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("01234123123", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
@pytest.mark.parametrize(
    "expect,phone",
    [
        (["0", "01", "012"], "  01  R2"),
        (["0", "01", "018", "0183"], "0k1 R.*8a3]"),
        (["0", "01", "018"], "018"),
        (["0", "01", "018"], "018"),
        (["0", "01", "018"], "Apple 0 Orange 1 Fruit 8"),
        (["0", "01"], "01R"),
        (["0", "01"], "0k1 R"),
        (["2", "23"], "a$2.3g"),
    ],
)
def test_search_phone_analyzer_filter(expect, phone):
    """A test to show how the phone number is tokenized."""
    index = SearchIndex(ContactIndex())
    index.drop_create()
    result = index.analyze("phone_number", phone)
    tokens = result["tokens"]
    assert expect == [x["token"] for x in tokens]


@pytest.mark.django_db
@pytest.mark.elasticsearch
@pytest.mark.parametrize(
    "criteria",
    [
        "01234",
        "01234 123 123",
        "01234123123",
        "06789",
        "06789 678 678",
        "06789678678",
    ],
)
def test_search_phone_two_number(criteria):
    # contact 1
    ContactFactory(company_name="", user=UserFactory())
    # contact 2 (should be found)
    contact_2 = ContactFactory(
        company_name="", user=UserFactory(first_name="c2")
    )
    ContactPhoneFactory(contact=contact_2, phone="01234 123 123")
    ContactPhoneFactory(contact=contact_2, phone="06789 678 678")
    # contact 3
    contact_3 = ContactFactory(
        company_name="", user=UserFactory(first_name="c3")
    )
    ContactPhoneFactory(contact=contact_3, phone="51111 111 111")
    ContactPhoneFactory(contact=contact_3, phone="57777 777 777")
    # test
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 3 == index.update()
    result, total = index.search(criteria, data_format=SearchFormat.ATTR)
    assert 1 == total, [(x.data.name, x.score) for x in result]
    assert ["c2"] == [x.data.name for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_partial_company_name():
    """Partial search for the company name.

    e.g. A search for ``rich`` will return ``Richard``

    https://www.kbsoftware.co.uk/crm/ticket/3532/

    """
    ContactFactory(company_name="Apple", user=UserFactory())
    contact = ContactFactory(company_name="Richard Evans", user=UserFactory())
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("rich", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_partial_first_name():
    """Partial search for the first name.

    e.g. A search for ``rich`` will return ``Richard``

    https://www.kbsoftware.co.uk/crm/ticket/3532/

    """
    ContactFactory(company_name="Apple", user=UserFactory())
    contact = ContactFactory(user=UserFactory(first_name="Richard"))
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("rich", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_partial_last_name():
    """Partial search for the last name.

    e.g. A search for ``rich`` will return ``Richard``

    https://www.kbsoftware.co.uk/crm/ticket/3532/

    """
    ContactFactory(company_name="Apple", user=UserFactory())
    contact = ContactFactory(user=UserFactory(last_name="Richard"))
    # should not find 'rich' in the middle of the name
    ContactFactory(user=UserFactory(last_name="TooRich"))
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 3 == index.update()
    result, total = index.search("rich", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_partial_username():
    """Partial search for 'user.username'.

    https://www.kbsoftware.co.uk/crm/ticket/3594/

    """
    ContactFactory(company_name="Apple", user=UserFactory())
    contact = ContactFactory(
        company_name="Orange", user=UserFactory(username="patrick")
    )
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("pat", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_postcode():
    c1 = ContactFactory(company_name="KB")
    ContactAddressFactory(contact=c1, postal_code="EX20 3CD")
    c2 = ContactFactory(company_name="Connect")
    ContactAddressFactory(contact=c2, postal_code="EX20 2AB")
    c3 = ContactFactory(company_name="Apple")
    ContactAddressFactory(contact=c3, postal_code="EX2 05B")
    c4 = ContactFactory(company_name="Apple Cake")
    ContactAddressFactory(contact=c4, postal_code="EX2 2AB")
    c5 = ContactFactory(company_name="Crab Apple")
    ContactAddressFactory(contact=c5, postal_code="TQ20 2AB")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 5 == index.update()
    result, total = index.search("EX20", data_format=SearchFormat.COLUMN)
    assert 3 == total
    assert [c2.pk, c1.pk, c3.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_postcode_ex203hu():
    """Criteria has no space in the postcode."""
    contact = ContactFactory(company_name="KB")
    ContactAddressFactory(contact=contact, postal_code="EX20 3HU")
    ContactAddressFactory(
        contact=ContactFactory(company_name="Apple"), postal_code="TQ20 2AB"
    )
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("EX203HU", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_postcode_ex20_3hu():
    """Criteria has a space in the postcode."""
    contact = ContactFactory(company_name="KB")
    ContactAddressFactory(contact=contact, postal_code="EX20 3HU")
    ContactAddressFactory(
        contact=ContactFactory(company_name="Apple"), postal_code="TQ20 2AB"
    )
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search("EX20 3HU", data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [contact.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_update():
    ContactFactory()
    ContactFactory()
    ContactFactory()
    search_index = SearchIndex(ContactIndex())
    search_index.drop_create()
    assert 3 == search_index.update()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_username():
    """Searching for ``sharon orange`` should match the second contact.

    The original code was matching the first contact, because the user-name was
    also ``sharon``.  To solve this issue we lowered the ``boost`` on the
    ``username`` field.

    """
    u1 = UserFactory(username="sharon", first_name="Sharon", last_name="Apple")
    c1 = ContactFactory(user=u1)
    u2 = UserFactory(username="pat", first_name="Sharon", last_name="Orange")
    c2 = ContactFactory(user=u2)
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search(
        "sharon orange", data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [c2.pk, c1.pk] == [x.pk for x in result]
