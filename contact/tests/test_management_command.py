# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from contact.search import ContactIndex
from contact.tests.factories import ContactAddressFactory, ContactFactory
from search.search import SearchIndex


def _create_contact():
    contact = ContactFactory(company_name="Applewood")
    ContactAddressFactory(contact=contact, postal_code="EX2 05B")


def _refresh_index():
    search_index = SearchIndex(ContactIndex())
    search_index.drop_create()
    assert 1 == search_index.update()


@pytest.mark.django_db
def test_init_app_contact():
    call_command("init_app_contact")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_contact_index():
    _create_contact()
    _refresh_index()
    call_command("rebuild_contact_index")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_refresh_contact_index():
    _create_contact()
    _refresh_index()
    call_command("refresh_contact_index")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_contact_index():
    _create_contact()
    _refresh_index()
    call_command("search_contact_index", "EX2")
