# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from taggit.models import Tag

from login.forms import UserNameForm
from .models import Contact, ContactAddress, ContactEmail, ContactPhone, Gender


class ContactExtraFormMixin(forms.ModelForm):
    """For editing contact email, address and phone number."""

    tag = forms.ModelMultipleChoiceField(
        label="Tag", queryset=Tag.objects.none(), required=False
    )

    def _full_width(self, *names):
        for name in names:
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})

    def _half_width(self, *names):
        for name in names:
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    def __init__(self, *args, **kwargs):
        all_tags = kwargs.pop("all_tags")
        show_tags = kwargs.pop("show_tags")
        super().__init__(*args, **kwargs)
        self.fields["notes"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 3}
        )
        if show_tags:
            tag_field = self.fields["tag"]
            tag_field.queryset = all_tags
            tag_field.widget.attrs.update(
                {"class": "chosen-select pure-input-1"}
            )
        else:
            del self.fields["tag"]


class ContactFormMixin(UserNameForm):
    """This form is used to create / update a contact with a primary address."""

    first_name = forms.CharField(
        max_length=Contact.MAX_LENGTH_FIRST_NAME, required=False
    )
    last_name = forms.CharField(
        max_length=Contact.MAX_LENGTH_LAST_NAME, required=False
    )
    email = forms.EmailField(required=False)

    def __init__(self, *args, **kwargs):
        self.update = kwargs.pop("update")
        super().__init__(*args, **kwargs)
        for name in self.fields:
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})
        if "notes" in self.fields:
            self.fields["notes"].widget.attrs.update(
                {"class": "pure-input-1", "rows": 3}
            )

    def clean(self):
        cleaned_data = super().clean()
        company_name = cleaned_data.get("company_name", "").strip()
        last_name = cleaned_data.get("last_name", "").strip()
        username = cleaned_data.get("username", "").strip()
        if username:
            raise_error = False
            try:
                user = get_user_model().objects.get(username=username)
                if self.update:
                    # is the 'username' used by this model instance?
                    raise_error = not self.instance.user.pk == user.pk
                else:
                    raise_error = True
            except get_user_model().DoesNotExist:
                pass
            if raise_error:
                raise forms.ValidationError(
                    "Another contact already has the '{}' username. "
                    "Please try a different one.".format(username),
                    code="contact__username__exists",
                )
        if not (company_name or last_name):
            raise forms.ValidationError(
                "Please enter a company name or last name",
                code="contact__company_or_last_name",
            )
        return cleaned_data


class ContactAddressEmptyForm(forms.ModelForm):
    class Meta:
        model = ContactAddress
        fields = ()


class ContactAddressForm(ContactExtraFormMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._full_width(
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
        )

    class Meta:
        model = ContactAddress
        fields = (
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
            "tag",
            "notes",
        )


class ContactEmailEmptyForm(forms.ModelForm):
    class Meta:
        model = ContactEmail
        fields = ()


class ContactEmailForm(ContactExtraFormMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._full_width("email", "notes")

    class Meta:
        model = ContactEmail
        fields = ("email", "notes", "tag")


class ContactEmptyForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ()


class ContactForm(ContactFormMixin, UserNameForm):
    phone = forms.CharField(max_length=50, required=False)
    mobile = forms.CharField(max_length=50, required=False)
    address_one = forms.CharField(
        max_length=100, label="Address", required=False
    )
    address_two = forms.CharField(max_length=100, label=" ", required=False)
    locality = forms.CharField(max_length=100, label=" ", required=False)
    town = forms.CharField(max_length=100, label="Town", required=False)
    admin_area = forms.CharField(max_length=100, label="County", required=False)
    postal_code = forms.CharField(
        max_length=20, label="Postcode", required=False
    )
    country = forms.CharField(max_length=100, label="Country", required=False)

    class Meta:
        model = Contact
        fields = (
            "username",
            "company_name",
            "first_name",
            "last_name",
            "phone",
            "mobile",
            "email",
            "address_one",
            "address_two",
            "locality",
            "town",
            "admin_area",
            "postal_code",
            "country",
            "website",
            "notes",
        )


class ContactMailingForm(ContactFormMixin):
    class Meta:
        model = Contact
        fields = (
            "company_name",
            "first_name",
            "last_name",
            "email",
            "username",
        )


class ContactPhoneEmptyForm(forms.ModelForm):
    class Meta:
        model = ContactPhone
        fields = ()


class ContactPhoneForm(ContactExtraFormMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._full_width("phone", "notes")

    class Meta:
        model = ContactPhone
        fields = ("phone", "notes")


class GenderForm(forms.ModelForm):
    class Meta:
        model = Gender
        fields = (
            "slug",
            "name",
            "pronoun_subject",
            "pronoun_object",
            "pronoun_dependent_possessive",
            "pronoun_independent_possessive",
        )
