# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import models
from django.urls import reverse
from reversion import revisions as reversion
from taggit.managers import TaggableManager
from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel


def check_perm_contact(user, contact):
    """Does this 'user' have permission to view the 'contact'?

    Keyword arguments:
    user -- the  logged in user
    contact -- the contact the user wants to view

    Check the following:

    1. Is the user a member of staff?
    2. Is the user trying to view their own contact information?
    3. Does the user have specific permission to view the contact
       (``UserContact``)?

    """
    result = user.is_staff
    if not result:
        result = contact.user == user
        if not result:
            try:
                UserContact.objects.get(user=user, contact=contact)
                result = True
            except UserContact.DoesNotExist:
                pass
        if not result:
            raise PermissionDenied(
                "User '{}' cannot access information for user '{}'".format(
                    user.username, contact.user.username
                )
            )
    return result


def max_length(x, max_length):
    if x is None:
        return ""

    return x[:max_length]


class ContactError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class GenderManager(models.Manager):
    def create_gender(self, slug, name):
        obj = self.model(slug=slug, name=name)
        obj.save()
        return obj

    def female(self):
        try:
            return self.model.objects.get(slug="F")
        except Gender.DoesNotExist:
            pass
        return None

    def male(self):
        try:
            return self.model.objects.get(slug="M")
        except Gender.DoesNotExist:
            pass
        return None


class Gender(models.Model):
    """
    Pronoun idea originally from reddit article:

    Modified with the help of Matt and Zoe to::

      they - subject
      them - object
      their - dependent_possessive
      theirs - independent_possessive

    """

    slug = models.CharField(max_length=10, blank=False)
    name = models.CharField(max_length=50, blank=False)
    pronoun_subject = models.CharField(
        max_length=50, help_text="he/she/they", blank=True
    )
    pronoun_object = models.CharField(
        max_length=50, help_text="him/her/them", blank=True
    )
    pronoun_dependent_possessive = models.CharField(
        max_length=50, help_text="his/her/their", blank=True
    )
    pronoun_independent_possessive = models.CharField(
        max_length=50, help_text="his/hers/theirs", blank=True
    )
    objects = GenderManager()

    class Meta:
        ordering = ("name",)
        verbose_name = "Gender"
        verbose_name_plural = "Genders"

    def __str__(self):
        return f"{self.name}"

    @property
    def male(self):
        return self.slug == "M"

    @property
    def female(self):
        return self.slug == "F"


class ContactManager(models.Manager):
    def _user_name_suffix(self, first_name, last_name, suffix):
        if first_name:
            result = "{}.{}{}".format(first_name, last_name, suffix)
        else:
            result = "{}{}".format(last_name, suffix)
        return result.strip(".").lower()

    def contact_emails(self, emails):
        """Check the emails and return any which are linked to a contact.

        .. note:: This method is required by apps which don't know the contact
                  model until runtime
                  (typically using ``CONTACT_MODEL`` and ``get_contact_model``).

        """
        return ContactEmail.objects.contact_emails(emails)

    def create_contact(self, user, **kwargs):
        obj = self.model(user=user, **kwargs)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def delegated_to(self, user):
        """List of contacts who have delegated their work to the 'user'.

        Some of our projects use their own ``Contact`` model with a ``delegate``
        field e.g::

          delegate = models.ForeignKey(
              "self",
              blank=True,
              null=True,
              related_name="contact_delegate",
              on_delete=models.CASCADE,
          )

        In this case, the ``delegated_to`` method would probably do this::

          return self.model.objects.filter(delegate__user=user)

        """
        return self.model.objects.none()

    def deleted_contacts(self):
        return self.model.objects.filter(deleted=True)

    def init_contact(self, user, **kwargs):
        try:
            x = self.model.objects.get(user=user)
        except self.model.DoesNotExist:
            x = self.create_contact(user, **kwargs)
        return x

    def os_fees(self):
        return []

    def create_unique_user(self, first_name, last_name):
        """Create a user with a unique user name."""
        suffix = ""
        index = 0
        found = True
        if first_name is None:
            first_name = ""
        if last_name is None:
            last_name = ""
        if not (first_name or last_name):
            raise ContactError(
                "To create a unique user you must "
                "enter a first name or last name"
            )

        while found:
            first = ""
            last = ""
            user_name = self._user_name_suffix(first_name, last_name, suffix)
            if len(user_name) > Contact.MAX_LENGTH_USERNAME:
                length = len(first_name) - (
                    len(user_name) - Contact.MAX_LENGTH_USERNAME
                )
                first = first_name[:length]
                user_name = self._user_name_suffix(first, last_name, suffix)
            if len(user_name) > Contact.MAX_LENGTH_USERNAME:
                user_name = self._user_name_suffix("", last_name, suffix)
            if len(user_name) > Contact.MAX_LENGTH_USERNAME:
                length = len(last_name) - (
                    len(user_name) - Contact.MAX_LENGTH_USERNAME
                )
                last = last_name[:length]
                user_name = self._user_name_suffix("", last, suffix)
            try:
                user = get_user_model().objects.get(username=user_name)
                index = index + 1
                suffix = ".{}".format(index)
            except ObjectDoesNotExist:
                found = False
        user = get_user_model().objects.create_user(user_name)
        user.first_name = max_length(first_name, Contact.MAX_LENGTH_FIRST_NAME)
        user.last_name = max_length(last_name, Contact.MAX_LENGTH_LAST_NAME)
        user.save()
        return user

    def matching_email(self, email):
        """Return a list of contacts with this email address."""
        qs = (
            ContactEmail.objects.current()
            .exclude(contact__deleted=True)
            .filter(email=email)
        )
        return set([x.contact for x in qs])

    # def pks_with_email(self, email, object_pks):
    #    """Find the IDs of contact records matching the email address.
    #    .. note:: Only look for contacts where the primary key is included in
    #              the list of ``object_pks``.
    #
    #    Keyword arguments:
    #    email -- find contacts matching this email address
    #    object_pks -- only include contacts from ``object_pks``
    #
    #    """
    #    return (
    #        ContactEmail.objects.current()
    #        .exclude(contact__deleted=True)
    #        .filter(contact__pk__in=object_pks)
    #        .filter(email=email)
    #        .values_list("contact__pk", flat=True)
    #    )

    def search(self, text, field_list=None):
        """Search for a contact using SQL.

        .. note:: This method is used in Malcolm's projects.
                  KB projects are using ElasticSearch.

        """
        if field_list is None:
            field_list = [
                "user__username",
                "user__first_name",
                "user__last_name",
                "user__email",
                "company_name",
                "addresses__address_one",
                "addresses__address_two",
                "addresses__locality",
                "addresses__town",
                "addresses__admin_area",
                "addresses__country",
                "addresses__postal_code",
                "phones__phone",
                "website",
                "position",
            ]
        words = text.split(" ")
        query = None
        for word in words:
            for field in field_list:
                contains_field_name = "{}__icontains".format(field)
                if query:
                    query = query | models.Q(**{contains_field_name: word})
                else:
                    query = models.Q(**{contains_field_name: word})
        return self.model.objects.filter(query).order_by("pk").distinct("pk")

    def user_in_a_group(self, user, group_pks):
        """Used by the workflow app - does a user belong to a group?"""
        result = False
        qs = user.groups.filter(pk__in=group_pks)
        if qs.exists():
            result = True
        return result


class Contact(TimedCreateModifyDeleteModel):
    """Contact (or Company) model.

    22/03/2018, Hangout with Tim::

      [pk] Ref Contact addresses... does the company name belong in the address
      or the contact details?  I think it is the address?
      [tb] Probably... the contact is an individual or a company...
      [pk] So perhaps it stays in the contact
      [tb] I would say Company is an entity
      [pk] A separate entity?
      [tb] It is like Contact
      [pk] Yes...
      I think it stays in contact for now.
      [tb] I think do what you said. Make "Contact" a "thing with an address"
      Could be a company, could be a person
      [pk] Yes...  and they can both have multiple addresses / locations.

    28/03/2018, We will remove the ``slug`` field and use the ``username``
    instead.  I don't think the slug has anything useful to do (although I
    might be wrong):

    - https://www.kbsoftware.co.uk/crm/ticket/1996/
    - https://www.kbsoftware.co.uk/crm/ticket/3117/

    """

    # Default consent for 'ContactCreateMailingView'
    GDPR_CONSENT_CONTACT_MAILING = "gdpr-consent-contact-mailing"
    # See 'django/contrib/auth/models.py'
    MAX_LENGTH_FIRST_NAME = 30
    MAX_LENGTH_LAST_NAME = 150
    MAX_LENGTH_USERNAME = 150

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    company_name = models.CharField(max_length=100, blank=True)
    title = models.CharField(max_length=30, blank=True)
    # address
    website = models.URLField(blank=True)
    # personal
    dob = models.DateField(blank=True, null=True)
    gender = models.ForeignKey(
        Gender, blank=True, null=True, on_delete=models.CASCADE
    )
    position = models.CharField(max_length=50, blank=True)
    notes = models.TextField(blank=True)
    objects = ContactManager()

    class JSONAPIMeta:
        resource_name = "contact"

    class Meta:
        ordering = ("user__last_name", "user__first_name")
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return "{}".format(self.full_name)

    def address(self, separator=None, exclude_postal_code=None, tag=None):
        result = ""
        address = self.current_address(tag)
        if address:
            result = address.address(separator, exclude_postal_code)
        return result

    @property
    def checkout_description(self):
        """For the 'checkout' app."""
        return ["Refresh Card Details"]

    @property
    def checkout_total(self):
        return Decimal()

    def current_address(self, tag=None):
        qs = self.current_addresses()
        if tag is None:
            pass
        else:
            qs = qs.filter(tags__name__in=[tag])
        return qs.order_by("-pk").first()

    def current_addresses(self):
        """All the addresses for a contact."""
        return self.addresses.exclude(deleted=True)

    def current_email(self):
        qs = self.current_emails()
        return qs.order_by("-pk").first()

    def current_emails(self):
        """All the addresses for a contact."""
        return self.emails.exclude(deleted=True)

    def current_phone(self, tag=None):
        qs = self.current_phones()
        if tag is None:
            pass
        else:
            qs = qs.filter(tags__name__in=[tag])
        return qs.order_by("-pk").first()

    def current_phones(self):
        """All the phone numbers for a contact."""
        return self.phones.exclude(deleted=True)

    def email(self):
        """Get the current email address for the contact."""
        result = ""
        contact_email = self.current_email()
        if contact_email:
            result = contact_email.email
        elif self.user.email:
            result = self.user.email
        return result

    @property
    def full_name(self):
        result = ""
        if self.company_name:
            result = self.company_name
        else:
            result = self.user.get_full_name()
            if self.title:
                result = "{} {}".format(self.title, result)
        if not result:
            result = self.user.username
        return result

    def get_full_name(self):
        """Temporary - while we wait for merge of '1559-contact-images'.

        02/06/2020, ``get_full_name`` is a method (not a property) in The
        Django ``User`` model.
        To avoid confusion, we will match this in our ``Contact`` model.

        """
        return self.full_name

    def get_absolute_url(self):
        return reverse("contact.detail", args=[self.pk])

    def get_short_name(self):
        """Used for views where the full name may be too long..."""
        return self.full_name

    def get_summary_description(self):
        return filter(None, (self.user.get_full_name(), self.company_name))

    def get_multi_as_dict(self):
        """Return the contact multi models as a dictionary.

        .. note:: For use in a view context.

        """
        return dict(
            contact_addresses=self.current_addresses().order_by("pk"),
            contact_emails=self.current_emails().order_by("pk"),
            contact_phones=self.current_phones().order_by("pk"),
        )

    def groups(self):
        return self.user.groups.all().order_by("name")

    def is_delegate_for(self, contact):
        return False

    def phone(self, tag=None):
        result = ""
        phone = self.current_phone(tag)
        if phone:
            result = phone.phone
        return result

    def pronoun_subject(self):
        result = "they"
        if self.gender and self.gender.pronoun_subject:
            result = self.gender.pronoun_subject
        return result

    def pronoun_object(self):
        result = "them"
        if self.gender and self.gender.pronoun_object:
            result = self.gender.pronoun_object
        return result

    def pronoun_dependent_possessive(self):
        result = "their"
        if self.gender and self.gender.pronoun_dependent_possessive:
            result = self.gender.pronoun_dependent_possessive
        return result

    def pronoun_independent_possessive(self):
        result = "theirs"
        if self.gender and self.gender.pronoun_independent_possessive:
            result = self.gender.pronoun_independent_possessive
        return result

    def set_deleted(self, user):
        super().set_deleted(user)
        self.user.is_active = False
        self.user.set_unusable_password()
        self.user.save()

    def undelete(self):
        super().undelete()
        self.user.is_active = True
        self.user.save()

    @property
    def landline_phone(self):
        return self.phone(tag=ContactPhone.LANDLINE)

    @property
    def mobile_phone(self):
        return self.phone(tag=ContactPhone.MOBILE)

    @property
    def invoice_address(self):
        return self.current_address(tag=ContactAddress.INVOICE)

    @property
    def primary_address(self):
        return self.current_address(tag=ContactAddress.PRIMARY)

    def workflow_groups(self):
        """Group details for the workflow app.

        The ``id`` / ``pk`` should be the group ID used by Flowable.

        """
        groups = self.groups()
        return {x.pk: x.name for x in groups}


reversion.register(Contact)


class ContactAddressManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_contact_address(self, contact):
        try:
            x = self.model.objects.get(contact=contact)
        except self.model.DoesNotExist:
            x = self.model(contact=contact)
            x.save()
        return x


class ContactAddress(TimedCreateModifyDeleteModel):
    """Contact address."""

    PRIMARY = "Primary"
    DELIVERY = "Delivery"
    INVOICE = "Invoice"

    contact = models.ForeignKey(
        Contact, related_name="addresses", on_delete=models.CASCADE
    )
    tags = TaggableManager(help_text="Address Type", blank=True)
    address_one = models.CharField("Address", max_length=100, blank=True)
    address_two = models.CharField(" ", max_length=100, blank=True)
    locality = models.CharField(" ", max_length=100, blank=True)
    town = models.CharField(max_length=100, blank=True)
    admin_area = models.CharField("County", max_length=100, blank=True)
    postal_code = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=100, blank=True)
    notes = models.TextField(blank=True)
    access = models.TextField(
        blank=True, help_text="Instructions for gaining access"
    )
    access_code = models.CharField(
        max_length=10, blank=True, help_text="Key code for gaining access"
    )
    objects = ContactAddressManager()

    class Meta:
        ordering = ["contact__user__username"]
        verbose_name = "Contact Address"
        verbose_name_plural = "Contact Addresses"

    def __str__(self):
        return "{} {}".format(self.contact.full_name, self.postal_code)

    def address(self, separator=None, exclude_postal_code=None):
        if separator is None:
            separator = ", "
        address_lines = []
        if self.address_one:
            address_lines.append(self.address_one)
        if self.address_two:
            address_lines.append(self.address_two)
        if self.locality:
            address_lines.append(self.locality)
        if self.town:
            address_lines.append(self.town)
        if self.admin_area:
            address_lines.append(self.admin_area)
        if self.postal_code and not exclude_postal_code:
            address_lines.append(self.postal_code)
        if self.country:
            address_lines.append(self.country)
        return separator.join(address_lines)

    def address_with_newline(self):
        return self.address(separator="\n")

    def tag_names(self):
        result = ""
        names = [x.name for x in self.tags.all().order_by("name")]
        if names:
            result = "({})".format(", ".join(names))
        return result


reversion.register(ContactAddress)


class ContactConnectionManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)


class ContactConnection(TimedCreateModifyDeleteModel):
    """Contact connections."""

    contact = models.ForeignKey(
        Contact, related_name="connections", on_delete=models.CASCADE
    )
    tags = TaggableManager(help_text="Type of email address", blank=True)
    connection = models.ForeignKey(
        Contact, related_name="connection", on_delete=models.CASCADE
    )
    notes = models.TextField(blank=True)
    objects = ContactConnectionManager()

    class Meta:
        ordering = ["contact__user__username"]
        verbose_name = "Contact connections"
        verbose_name_plural = "Contact connections"

    def __str__(self):
        return "{} to {}".format(
            self.contact.full_name, self.connection.full_name
        )


reversion.register(ContactConnection)


class ContactEmailManager(models.Manager):
    def contact_emails(self, emails):
        """Check the emails and return any which are linked to a contact."""
        qs = self.model.objects.filter(email__in=emails).exclude(deleted=True)
        return [x.email for x in qs]

    def create_contact_email(self, contact, email):
        x = self.model(contact=contact, email=email)
        x.save()
        return x

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_contact_email(self, contact):
        try:
            x = self.model.objects.get(contact=contact)
        except self.model.DoesNotExist:
            x = self.model(contact=contact)
            x.save()
        return x


class ContactEmail(TimedCreateModifyDeleteModel):
    """Contact email addresses."""

    contact = models.ForeignKey(
        Contact, related_name="emails", on_delete=models.CASCADE
    )
    tags = TaggableManager(help_text="Type of email address", blank=True)
    email = models.EmailField()
    notes = models.TextField(blank=True)
    objects = ContactEmailManager()

    class Meta:
        ordering = ["contact__user__username"]
        verbose_name = "Contact email Address"
        verbose_name_plural = "Contact email Address"

    def __str__(self):
        return "{} {}".format(self.contact.full_name, self.email)

    def tag_names(self):
        result = ""
        names = [x.name for x in self.tags.all().order_by("name")]
        if names:
            result = "({})".format(", ".join(names))
        return result


reversion.register(ContactEmail)


class ContactPhoneManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_contact_phone(self, contact):
        try:
            x = self.model.objects.get(contact=contact)
        except self.model.DoesNotExist:
            x = self.model(contact=contact)
            x.save()
        return x


class ContactPhone(TimedCreateModifyDeleteModel):
    """Contact phone numbers."""

    FAX = "Fax"
    MOBILE = "Mobile"
    LANDLINE = "Landline"

    contact = models.ForeignKey(
        Contact, related_name="phones", on_delete=models.CASCADE
    )
    tags = TaggableManager(help_text="Type of Phone Number", blank=True)
    phone = models.CharField(max_length=50)
    notes = models.TextField(blank=True)
    objects = ContactPhoneManager()

    class Meta:
        ordering = ["contact__user__username"]
        verbose_name = "Contact Phone Number"
        verbose_name_plural = "Contact Phone Numbers"

    def __str__(self):
        return "{} {}".format(self.contact.full_name, self.phone)

    def tag_names(self):
        result = ""
        names = [x.name for x in self.tags.all().order_by("name")]
        if names:
            result = "({})".format(", ".join(names))
        return result


reversion.register(ContactPhone)


class UserContactManager(models.Manager):
    def create_user_contact(self, user, contact):
        obj = self.model(user=user, contact=contact)
        obj.save()
        return obj

    def for_user(self, user):
        return self.model.objects.filter(user=user)


class UserContact(TimeStampedModel):
    """A user can view information for a list of contacts.

    Used by ``check_perm_contact`` to see if a user has permission to view
    contact information.

    .. note:: If the user is a member of staff, they can view all contacts.

    .. note:: A user can view their own contact information.

    e.g.

    Andy - ConnexionSW
    Fred - ConnexionSW
    Andy - British Sugar
    Kate - British Sugar

    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    objects = UserContactManager()

    class Meta:
        ordering = ["user__username", "contact__user__username"]
        unique_together = ("user", "contact")
        verbose_name = "User Contact"
        verbose_name_plural = "User Contacts"

    def __str__(self):
        return "{} - {}".format(self.user.username, self.contact.full_name)


reversion.register(UserContact)
