# -*- encoding: utf-8 -*-
from django.db.models import Case, When
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from search.models import SearchFormat
from .models import Contact, ContactAddress
from .search import ContactIndex
from .serializers import (
    ContactAddressSerializer,
    ContactConnectionSerializer,
    ContactEmailSerializer,
    ContactPhoneSerializer,
    ContactSerializer,
)


class SoftDeleteViewSet(viewsets.ModelViewSet):
    """An API view which marks records as deleted, rather than deleting them."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def perform_destroy(self, instance):
        instance.set_deleted(self.request.user)


class ContactAddressViewSet(SoftDeleteViewSet):
    serializer_class = ContactAddressSerializer

    def get_queryset(self):
        return ContactAddress.objects.current()


class ContactConnectionViewSet(SoftDeleteViewSet):
    serializer_class = ContactConnectionSerializer

    def get_queryset(self):
        connection_id = self.request.query_params.get("connection_id")
        if connection_id:
            qs = ContactConnection.objects.current().filter(
                connection__pk=connection_id
            )
        else:
            qs = ContactConnection.objects.current()
        return qs


class ContactEmailViewSet(SoftDeleteViewSet):
    serializer_class = ContactEmailSerializer

    def get_queryset(self):
        return ContactEmail.objects.current()


class ContactPhoneViewSet(SoftDeleteViewSet):
    serializer_class = ContactPhoneSerializer

    def get_queryset(self):
        return ContactPhone.objects.current()


class ContactViewSet(SoftDeleteViewSet):
    serializer_class = ContactSerializer

    def get_queryset(self):
        from search.search import SearchIndex

        criteria = self.request.query_params.get("criteria")
        if criteria:
            index = SearchIndex(ContactIndex())
            result, _ = index.search(criteria, data_format=SearchFormat.COLUMN)
            pks = []
            for row in result:
                pks.append(row.pk)
            preserved = Case(
                *[When(pk=pk, then=pos) for pos, pk in enumerate(pks)]
            )
            qs = (
                Contact.objects.current().filter(pk__in=pks).order_by(preserved)
            )
        else:
            qs = Contact.objects.current().order_by("-pk")
        return qs

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response
