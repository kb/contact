# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from contact.models import Contact
from gdpr.models import Consent


class Command(BaseCommand):
    help = "Initialise contact app"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        consent = Consent.objects.init_consent(
            Contact.GDPR_CONSENT_CONTACT_MAILING, "Contact Mailing", "", False
        )
        self.stdout.write(f"- Created: {consent}")
        self.stdout.write(f"{self.help} - Complete")
