# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from contact.tasks import rebuild_contact_index


class Command(BaseCommand):
    help = "Rebuild contact index"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        rebuild_contact_index()
        self.stdout.write("{} - Complete".format(self.help))
