# -*- encoding: utf-8 -*-
import attr

from contact.models import Contact
from search.search import Fmt


@attr.s
class Row:
    """For use with 'data_as_attr'."""

    address = attr.ib()
    email = attr.ib()
    name = attr.ib()
    postcode = attr.ib()


class ContactIndexMixin:
    def _full_name(self, row):
        result = ""
        full_name = row.user.get_full_name()
        if row.title:
            result = "{} ".format(row.title)
        if full_name or row.title:
            result = "{}{}".format(result, full_name)
        if row.company_name:
            result = "{}: {}".format(row.company_name, result)
        return result

    def _partial_name(self, row):
        result = []
        if row.company_name:
            result.append(row.company_name)
        if row.user.first_name:
            result.append(row.user.first_name)
        if row.user.last_name:
            result.append(row.user.last_name)
        if row.user.username:
            result.append(row.user.username)
        return result

    def settings(self):
        """Index settings.

        ``phone_number``:

          The ``phone_number`` analyzer is a quick solution to searching for
          phone numbers: https://www.kbsoftware.co.uk/crm/ticket/3594/

          I started by trying to copy this example,
          https://stackoverflow.com/questions/42484892/elasticsearch-and-word-delimiter-token-filter
          but I couldn't get it to work.

          My final *solution* was to re-use existing analyzers.

        """
        return {
            "number_of_shards": 1,
            "analysis": {
                "analyzer": {
                    "autocomplete": {
                        "tokenizer": "autocomplete",
                        "filter": ["lowercase", "whitespace_remove"],
                    },
                    "autocomplete_search": {
                        "tokenizer": "keyword",
                        "filter": ["lowercase", "whitespace_remove"],
                    },
                    "email": {"tokenizer": "uax_url_email"},
                    "phone_number": {
                        "tokenizer": "autocomplete",
                        "char_filter": ["digit_only"],
                        "filter": ["unique"],
                    },
                },
                "char_filter": {
                    "digit_only": {
                        "type": "pattern_replace",
                        "pattern": "[^0-9]",
                        "replacement": "",
                    }
                },
                "filter": {
                    "whitespace_remove": {
                        "type": "pattern_replace",
                        "pattern": " ",
                        "replacement": "",
                    }
                },
                "tokenizer": {
                    "autocomplete": {
                        "type": "edge_ngram",
                        "min_gram": 1,
                        "max_gram": 20,
                        "token_chars": ["digit", "letter", "whitespace"],
                    }
                },
            },
        }

    def mappings(self):
        """Index configuration.  Used by the Elasticsearch 'create' method.

        Create an index with an explicit mapping
        https://www.elastic.co/guide/en/elasticsearch/reference/8.8/explicit-mapping.html#create-mapping

        """
        return {
            "properties": {
                "address": {"type": "text", "analyzer": "english"},
                "email": {"type": "text", "analyzer": "email"},
                "full_name": {"type": "text", "analyzer": "english"},
                "is_deleted": {"type": "boolean"},
                "partial_name": {
                    "type": "text",
                    "analyzer": "autocomplete",
                    "search_analyzer": "autocomplete_search",
                    "fields": {"keyword": {"type": "keyword"}},
                },
                "phone": {"type": "text", "analyzer": "phone_number"},
                "postcode": {
                    "type": "text",
                    "analyzer": "autocomplete",
                    "search_analyzer": "autocomplete_search",
                    "fields": {"keyword": {"type": "keyword"}},
                },
                "username": {"type": "text", "analyzer": "standard"},
            },
        }

    def data_as_attr(self, source, pk):
        """A row of data returned as part of the search results.

        .. note:: Check the ``source`` method.  Not all fields are added to the
                  Elasticsearch index.

        """
        return Row(
            address="{address}".format(**source),
            email="{email}".format(**source),
            name="{full_name}".format(**source),
            postcode="{postcode}".format(**source),
        )

    def data_as_fmt(self, source, pk):
        """A row of data returned as part of the search results."""
        return [
            [Fmt("{full_name}".format(**source), url=True)],
            [Fmt("{address} {postcode}".format(**source))],
            [Fmt("{email}".format(**source), url=True)],
        ]

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "contact"

    def query(self, criteria):
        """The Elasticsearch query."""
        query = {
            "should": [
                {"match": {"address": {"query": criteria, "boost": 2}}},
                {"match": {"email": {"query": criteria, "boost": 25}}},
                {"match": {"full_name": {"query": criteria, "boost": 2}}},
                {"match": {"partial_name": {"query": criteria, "boost": 1}}},
                {"match": {"phone": {"query": criteria, "boost": 3}}},
                {"match": {"postcode": {"query": criteria, "boost": 6}}},
                {"match": {"username": {"query": criteria, "boost": 1}}},
            ]
        }
        return {
            "query": {"bool": query},
            "sort": ["_score", {"postcode.keyword": "asc"}],
        }

    def queryset(self, pk):
        """Build the Elasticsearch index using this queryset."""
        if pk is None:
            qs = Contact.objects.all().order_by("pk")
        else:
            qs = Contact.objects.filter(pk=pk)
        return qs

    def source(self, row):
        """Build the index using this data from the 'queryset'.

        .. note:: The full name includes the company name
                  (see the ``Contact`` model for more information).

        """
        qs = row.current_addresses()
        addresses = [x.address(exclude_postal_code=True) for x in qs]
        postcodes = [x.postal_code for x in qs]
        qs = row.current_emails()
        # email address does not index correctly unless lower case!?
        # https://www.kbsoftware.co.uk/crm/ticket/6155/
        emails = [x.email.lower() for x in qs]
        qs = row.current_phones()
        phone_numbers = [x.phone for x in qs]
        return {
            "address": ", ".join(addresses),
            "email": ", ".join(emails),
            "full_name": self._full_name(row),
            "partial_name": ", ".join(self._partial_name(row)),
            "phone": ", ".join(phone_numbers),
            "postcode": ", ".join(postcodes),
            "username": row.user.username,
        }

    def table_headings(self):
        """Table headings for the results."""
        return ["Name", "Address", "email"]

    def url_name(self):
        """URL name for the search results."""
        return "contact.detail"


class ContactIndex(ContactIndexMixin):
    @property
    def is_soft_delete(self):
        return False
