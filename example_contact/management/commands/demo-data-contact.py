# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from faker import Faker

from contact.models import Contact, ContactAddress, ContactEmail, ContactPhone


class Command(BaseCommand):
    help = "Demo data for the contact app"

    def _create_contact(self, name):
        fake = Faker()
        user = Contact.objects.create_unique_user(name[0], name)
        user.first_name = fake.first_name()
        user.last_name = fake.last_name()
        user.save()
        contact = Contact.objects.create_contact(user=user)
        contact.company_name = fake.company()
        contact.title = fake.prefix()
        contact.website = fake.url()
        contact.position = fake.job()[50:]
        contact.notes = fake.sentence()
        contact.save()
        address = ContactAddress(
            contact=contact, town="Hatherleigh", notes=name
        )
        address.save()
        address.tags.add("Delivery", "Invoice")
        address = ContactAddress(contact=contact, town="Okehampton", notes=name)
        address.save()
        email = ContactEmail(contact=contact, email=fake.email())
        email.save()
        email.tags.add("Home", "Work")
        email = ContactEmail(contact=contact, email=fake.email())
        email.save()
        phone = ContactPhone(contact=contact, phone=fake.phone_number())
        phone.save()
        phone = ContactPhone(contact=contact, phone=fake.phone_number())
        phone.save()
        phone.tags.add("Home", "Work")
        return contact

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        for name in (
            "000",
            "111",
            "222",
            "333",
            "444",
            "555",
            "666",
            "777",
            "888",
            "999",
            "AAA",
            "BBB",
            "CCC",
            "DDD",
            "EEE",
            "FFF",
            "GGG",
            "HHH",
            "Apple",
            "Cherry",
            "Lemon",
            "Orange",
            "Pear",
        ):
            contact = self._create_contact(name)
            self.stdout.write(f"{contact.user.username} - {contact}")
        self.stdout.write(f"{self.help} - Complete")
