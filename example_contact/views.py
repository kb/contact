# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, ListView, TemplateView

from base.view_utils import BaseMixin
from contact.search import ContactIndex
from contact.views import ContactDetailMixin, ContactListMixin
from search.views import SearchViewMixin


class ContactDetailView(
    LoginRequiredMixin, ContactDetailMixin, BaseMixin, DetailView
):
    template_name = "example/contact_detail.html"


class ContactListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactListMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/contact_list.html"


class DashView(TemplateView):
    template_name = "example/dash.html"


class HomeView(TemplateView):
    template_name = "example/home.html"


class SearchView(LoginRequiredMixin, StaffuserRequiredMixin, SearchViewMixin):
    INDEX_CHOICES = (("contact", "Contact"),)
    INDEX_CLASSES = {"contact": ContactIndex}
    paginate_by_for_search = 10


class SettingsView(TemplateView):
    template_name = "example/settings.html"
