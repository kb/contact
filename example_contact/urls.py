# -*- encoding: utf-8 -*-
from django.conf import settings
from django.urls import include, path, re_path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework import routers
from rest_framework.authtoken import views

from contact.api import (
    ContactAddressViewSet,
    ContactEmailViewSet,
    ContactPhoneViewSet,
    ContactViewSet,
)
from .views import (
    ContactDetailView,
    ContactListView,
    DashView,
    HomeView,
    SearchView,
)


router = routers.DefaultRouter()
router.register(r"addresses", ContactAddressViewSet, basename="address")
router.register(r"contacts", ContactViewSet, basename="contact")
router.register(r"emails", ContactEmailViewSet, basename="email")
router.register(r"phones", ContactPhoneViewSet, basename="phone")


urlpatterns = [
    # DOES NOT USE A TRAILING SLASH
    # http://localhost:8000/api/0.1/contact
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(r"^settings/$", view=HomeView.as_view(), name="project.settings"),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(
        r"^example/contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(
        r"^example/contact/$",
        view=ContactListView.as_view(),
        name="contact.list",
    ),
    re_path(
        r"^example/contact/mailing/$",
        view=ContactListView.as_view(),
        name="contact.mailing.list",
    ),
    re_path(
        r"^example/search/$",
        view=SearchView.as_view(),
        name="project.search",
    ),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
