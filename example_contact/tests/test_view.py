# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from contact.search import ContactIndex
from contact.tests.factories import (
    ContactAddressFactory,
    ContactEmailFactory,
    ContactFactory,
    ContactPhoneFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from search.search import SearchIndex


@pytest.mark.django_db
def test_contact_detail(client, django_assert_num_queries):
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    address_1 = ContactAddressFactory(contact=contact)
    address_2 = ContactAddressFactory(contact=contact)
    email_1 = ContactEmailFactory(contact=contact)
    email_2 = ContactEmailFactory(contact=contact)
    phone_1 = ContactPhoneFactory(contact=contact)
    phone_2 = ContactPhoneFactory(contact=contact)
    with django_assert_num_queries(14):
        response = client.get(reverse("contact.detail", args=[contact.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "contact_addresses" in response.context
    assert "contact_emails" in response.context
    assert "contact_phones" in response.context
    assert "next" in response.context
    contact_addresses = response.context["contact_addresses"]
    contact_emails = response.context["contact_emails"]
    contact_phones = response.context["contact_phones"]
    assert [address_1.pk, address_2.pk] == [x.pk for x in contact_addresses]
    assert [email_1.pk, email_2.pk] == [x.pk for x in contact_emails]
    assert [phone_1.pk, phone_2.pk] == [x.pk for x in contact_phones]
    assert "" == response.context["next"]


@pytest.mark.django_db
def test_contact_detail_next(client):
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    response = client.get(
        url_with_querystring(
            reverse("contact.detail", args=[contact.pk]),
            next=reverse("project.dash"),
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "next" in response.context
    assert reverse("project.dash") == response.context["next"]


@pytest.mark.django_db
def test_contact_list(client, django_assert_num_queries):
    u = UserFactory(is_staff=True)
    # login
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # setup
    ContactFactory(user=UserFactory(username="beta"))
    contact = ContactFactory(user=UserFactory(username="alpha"))
    ContactAddressFactory(contact=contact)
    ContactAddressFactory(contact=contact)
    ContactEmailFactory(contact=contact)
    ContactEmailFactory(contact=contact)
    ContactPhoneFactory(contact=contact)
    ContactPhoneFactory(contact=contact)
    # test
    with django_assert_num_queries(12):
        response = client.get(reverse("contact.list"))
    assert 200 == response.status_code
    assert ["alpha", "beta"] == [
        obj.user.username for obj in response.context["object_list"]
    ]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search(client):
    # login
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # setup
    contact = ContactFactory(
        company_name="KB", user=UserFactory(first_name="P", last_name="Kimber")
    )
    ContactAddressFactory(
        contact=contact,
        address_one="1 Market Street",
        town="Hatherleigh",
        postal_code="EX20 3CD",
    )
    ContactEmailFactory(contact=contact, email="web@pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 1 == index.update()
    url = url_with_querystring(reverse("project.search"), criteria="EX20")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    object_list = response.context["object_list"]
    assert [contact.pk] == [x.pk for x in object_list]
