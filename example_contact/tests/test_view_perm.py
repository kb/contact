# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import UserContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_contact_detail(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_detail_for_user(client):
    """Login as the client (user)."""
    user_contact = UserContactFactory()
    assert (
        client.login(
            username=user_contact.contact.user.username, password=TEST_PASSWORD
        )
        is True
    )
    response = client.get(
        reverse("contact.detail", args=[user_contact.contact.pk])
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_contact_list(perm_check):
    perm_check.staff(reverse("contact.list"))


@pytest.mark.django_db
def test_search(perm_check):
    url = reverse("project.search")
    perm_check.staff(url)
